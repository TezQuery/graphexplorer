# The TezQuery Graph Explorer

This repository contains the source code of the TezQuery Graph Explorer.
 The TezQuery Graph Explorer exposes the Tezos blockchain as a graph structure, and enables its semi-automatic and interactive exploration.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/RXfbaN2oIFU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## The Tezos Blockchain Graph
    
  <img src="runtime/webui/sample3.png" width="480">

  The graph shows accounts (*nodes*) connected by transactions (*directed edges*). The exploration of the blockchain graph makes it possible to observe transaction patterns, to discover clusters (like sets of transactions between the same groups of accounts), to examine sequences of transactions between accounts and contracts, etc.
## Exploration Modes and Tools

Four modes for graph exploration are possible:
  * `Last transactions`: show the graph of the most recent transactions.
  * `Biggest transactions`: explore the transactions of greatest amounts over a given time period (1 day, 1 month, 10mins, etc.).
  * `Indirect transactions`: determine whether two accounts are connected either directly or indirectly by a sequence of transactions (possibly involving contracts).
  * `Explore from an account`: discover follow-up transactions starting from a given account, by semi-automatic expansion using adjustable criteria.

  <img src="runtime/webui/help/img/explorations/backbone.png" width="172">
  <img src="runtime/webui/help/img/explorations/accountsandcontracts.png" width="200">
  <img src="runtime/webui/help/img/explorations/from1account2.png" width="235">

Each kind of exploration comes with a set of common facilities to select, drag or zoom subparts of the blockchain graph. One can also automatically regroup singles, select arbitrary subsets of accounts to be merged, splitted, removed, etc. or focus on the backbone structure of the transactions. This is done using the controls on the main menu bar that are used to automatically generate the corresponding high-level queries that retrieve all the required information.
Each kind of exploration also comes with its own specific set of parameters, such as a range of dates, an expansion ratio for controling the semi-automatic recursive exploration, etc.



## Documentation

  * Know how to interpret the [Tezos graph representation](graph-representation.md) (graph legend)
  * Learn how to use the explorer [controls](menubar.md)
  * See sample [explorations](exploration-kinds.md)
  
## Running the code

See the [instructions on how to install and run](runtime/README.md) the Graph Explorer within the `runtime` folder above.

This is a development by the TezQuery team: feel free to [contact](mailto:tezquery@gmail.com) us.




