# Blockchain Graph Explorations:

* Browsing the graph of the `latest transactions` to discover the last most active Tezos actors and recent transaction patterns.
* Exploring how the `biggest transactions` over a given period of time are connected.
* Searching whether two accounts are connected by a sequence of `indirect transactions`, possibly involving contracts.
* Exploring the blockchain `starting from a given account`.

Sample Explorations
-------------------

### Identifying sequences of transactions that connect two account clusters:

![exploration sample screenshot](runtime/webui/help/img/explorations/semiauto.png)

### Exploring the backbone of a Tezos graph:

![exploration sample screenshot](runtime/webui/help/img/explorations/backbone.png)

### How the biggest Tezos transactions ever made on the Tezos blockchain are connected:

![exploration sample screenshot](runtime/webui/help/img/explorations/biggest5.png)

### Exploration from an account:

![exploration sample screenshot](runtime/webui/help/img/explorations/accountsandcontracts.png) ![exploration sample screenshot](runtime/webui/help/img/explorations/from1account2.png)

### Search for indirect transactions:

Sample query: is some address X connected to another address Y (considered suspicious) by 4 hops of transactions with large amounts?

This can be expressed using the Tezquery tool, either through the user interface or by passing directly parameters on the URLs as follows (when the Graph Explorer is running on your local machine):

[http://localhost:8080/indirect_transactions.html?src=tz1iBvy8aujWLAfDpS7mbmdURwYzUfvxsHmr&trg=KT18yTsDxUbVrenxZsbFSx6Ai72hRHod9pHV&depth=5&min-amount=20000](http://localhost:8080/indirect_transactions.html?src=tz1iBvy8aujWLAfDpS7mbmdURwYzUfvxsHmr&trg=KT18yTsDxUbVrenxZsbFSx6Ai72hRHod9pHV&depth=5&min-amount=20000)

![exploration sample screenshot](runtime/webui/help/img/explorations/indirect.png)
