# The TezQuery Graph Representation

The TezQuery graph explorer exposes the Tezos blockchain as a graph data structure where accounts are linked by transactions. Two kinds of Tezos accounts can hold tokens and can be transaction source or destination:

* implicit accounts (addresses starting with "tz1", "tz2", or "tz3") that are represented as blue nodes in the graph;
* smart contracts (addresses starting with "KT1") that are represented as orange nodes in the graph.

A transaction is seen as a directed edge between two accounts. It is represented as a gray arrow starting from the source account (the sender) and pointing to the target account (the transaction destination). For instance:

|     |     |
| --- | --- |
| <img alt="graph pattern" width="80" style="width:80px;" src="runtime/webui/help/img/graph-representation/pattern0.png" /> | A single transaction between an implicit account and a smart contract, where the implicit account sends tokens to the smart contract. |
| <img alt="graph pattern" width="120"  style="width:120px;" src="runtime/webui/help/img/graph-representation/pattern1.png" /> | A pattern with 4 transactions in which one implicit account (in the middle) receives tokens from 3 different smart contracts, and sends tokens to another implicit account. |
| <img alt="graph pattern" width="120" style="width:120px;" src="runtime/webui/help/img/graph-representation/pattern2.png" /> | A pattern with 7 transactions between 8 implicit accounts, where a single account (in the middle) sends tokens to 7 other accounts. |

Clicking on an account in the graph displays more information such as the addresses of related accounts, and (in the property window) its own address, the current XTZ balance and metadata information when available. ![property window](runtime/webui/help/runtime/webui/help/img/graph-representation/pw.png)


## Cluster representations


When using [advanced exploration features](exploration-kinds.md), the graph explorer also uses cluster representations to simplify the layout and facilitate the graph exploration. Clusters can be either homogeneous (an homogeneous cluster regroups accounts of the same kind) or mixed (when it regroups at least one implicit account and one smart contract, and possibly more of each kind). For instance, you may encounter the following representations:

|     |     |
| --- | --- |
| <img alt="graph pattern" width="80" style="width:80px;" src="runtime/webui/help/img/graph-representation/implicit-cluster2b.png" /> | Cluster regrouping 2 implicit accounts. |
| <img alt="graph pattern" width="120" style="width:120px;" src="runtime/webui/help/img/graph-representation/implicit-3-cluster.png" /> | Cluster of 3 implicit accounts from which tokens were sent to another single account. |
| <img alt="graph pattern" width="120" style="width:120px;" src="runtime/webui/help/img/graph-representation/implicit-4-cluster.png" /> | Cluster of 4 implicit accounts connected (in both directions) to another account. |
| <img alt="graph pattern" width="90" style="width:90px;" src="runtime/webui/help/img/graph-representation/implicit-small-cluster.png" /> | Cluster of 7 implicit accounts. |
| <img alt="graph pattern" width="120" style="width:120px;" src="runtime/webui/help/img/graph-representation/implicit-medium-cluster.png" /> | Homogeneous cluster of implicit accounts with a transaction to another account. |
| <img alt="graph pattern" width="120" style="width:120px;" src="runtime/webui/help/img/graph-representation/homogeneous-cluster-smart.png" /> | Homogeneous cluster of smart contracts with tokens sent to an implicit account. |
| <img alt="graph pattern" width="40" style="width:40px;" src="runtime/webui/help/img/graph-representation/smart-2-mixed.png" /> | Cluster of 2 smart contracts with tokens sent to an implicit account. |
| <img alt="graph pattern" width="110" style="width:110px;" src="runtime/webui/help/img/graph-representation/heterogeneous-cluster24b.png" /> | Mixed cluster containing 2 smart contracts and 4 implicit accounts. |
| <img alt="graph pattern" width="200" style="width:200px;" src="runtime/webui/help/img/graph-representation/medium-mixed-cluster.png" /> | Medium-size mixed cluster containing a rather balanced number of implicit accounts and smart contracts, with transaction to an implicit account. |
| <img alt="graph pattern" width="260" style="width:260px;" src="runtime/webui/help/img/graph-representation/large-mixed-cluster.png" /> | Large mixed cluster containing a high proportion of implicit accounts and few smart contracts, with transaction to a single smart contract. |

Cluster representations can be formed automatically by clicking on the "[group singles](menubar.md)" button on the main [menu bar](menubar.md). Custom clusters can also be created using the "[select](menubar.md)" and then the "[merge](menubar.md)" tools of the menu bar. Any cluster can be split at anytime by using the "[select](menubar.md)" and then the "[split](menubar.md)" tools (see the [menu bar help](menubar.md) for more details).

## Exploration from one account

When [exploring from one account](exploration-kind.md), the origin of the exploration (which can be an account of any kind) is marked in green, for instance:

|     |     |
| --- | --- |
| <img alt="graph pattern" width="300" style="width:300px;" src="runtime/webui/help/img/graph-representation/exploration-origin.png" /> | A sample Tezos graph with exploration origin. |
