# TezQuery Graph Explorer User Interface Controls

The menu bar provides a set of common tools as well as exploration-specific parameters to control the graph exploration:

<img alt="menu bar screenshot" width="800" src="runtime/webui/help/img/menu-bar/menubar.png">


## Common tools

### Dragging, zooming OR selecting portions of the graph

It is possible and often useful to switch between the two following modes:

<img alt="UI screenshot" width="170" src="runtime/webui/help/img/menu-bar/dragnzoom.png">

The drag & zoom (default) mode allows one to unzoom the graph in order to have a more complete overview, or to zoom on portions of the graph. For zooming and unzooming, you can either use the wheel of compatible mouse devices, or touch the screen with two fingers on compatible devices.  
Even more importantly, this mode also allows one to drag and move either the whole graph or portions of it. For moving a portion of the graph, it is possible to click and drag any account to another place, in which case all its associated transactions will automatically follow while progressively reaching the new desired layout. This is often useful for isolating and focusing on a specific subgraph.

<img alt="UI screenshot" width="170" src="runtime/webui/help/img/menu-bar/select.png">

The select mode allows one to select a group of accounts by clicking and moving the cursor and then releasing once the groups of accounts has been selected. Selecting a group of accounts is useful for displaying their addresses/names, and also for the selected accounts to be merged, splitted, or removed (using the following buttons).

* * *

### Merge, Split, Remove

<img alt="UI screenshot" width="74" src="runtime/webui/help/img/menu-bar/merge.png">

This regroups the set of accounts selected (using the select tool above), resulting in a [cluster representation](graph-representation.md).

<img alt="UI screenshot" width="59" src="runtime/webui/help/img/menu-bar/split.png">

This splits a given [cluster representation](graph-representation.md) into a subgraph with explicit transactions between individual accounts. Splitting assumes that a given cluster (previously created, either automatically or manually) has first been selected with the aforementioned select tool.

<img alt="UI screenshot" width="82" src="runtime/webui/help/img/menu-bar/remove.png">

This deletes the set of selected accounts (possibly including [cluster representations](graph-representation.md)) from the current graph view. Removing assumes that a non-empty set of accounts has first been selected with the aforementioned select tool. Warning: removed nodes can only be recovered with a full update of the graph view (see below).

* * *

### Group Singles, Backbone

<img alt="UI screenshot" width="117" src="runtime/webui/help/img/menu-bar/groupsingles.png">

This automatically regroups all the "single" accounts into [cluster representations](graph-representation.md). An account (either implicit or smart contract) is considered "single" whenever it is connected only to a single other node. Grouping singles means that: for each account x in the graph, all the accounts that are connected to x (and only to x) are regrouped together within a [cluster representation](graph-representation.md).

<img alt="UI screenshot" width="94" src="runtime/webui/help/img/menu-bar/backbone.png">

This rearranges the current graph view so as to emphasize chains of transactions. This can be used independently or in conjunction with grouping singles so as to automatically obtain a clearer view of how accounts are connected together, especially in highly connected graphs.

* * *

### Number of transactions

<img alt="UI screenshot" width="225" src="runtime/webui/help/img/menu-bar/nbtransactions.png">

This indicates the maximum number of transactions to be displayed (e.g. an upper bound on the size of the displayed graph). On tzquery.io, the maximum number of transactions is intentionally limited to several hundreds of transactions. If you want to observe significantly more (e.g. thousands of transactions), you can do so by deploying Tezquery in hosted mode, starting from the [source code](https://gitlab.com/TezQuery/graphexplorer). In hosted mode, the maximum number of transactions is potentially unlimited (e.g. limited only by your hardware device's computing power).

<img alt="UI screenshot" width="81" src="runtime/webui/help/img/menu-bar/update.png">

This clears and updates the current view of the graph according to the given number of transactions, and potentially other parameters such as range of dates (see below).


## Exploration-specific parameters on the menu bar

Each [kind of exploration](exploration-kinds.html) comes with specific additional parameters:

Range of dates: <img alt="UI screenshot" width="458" src="runtime/webui/help/img/menu-bar/rangeofdates.png"
          style="align-items: center; display:flex;"> (when searching for biggest transactions, for indirect transactions, and from one account)

Specifying a range of dates is optional and allows one to focus on a subpart of the blockchain. By default, whenever the date fields are not specified, the whole Tezos blockchain is considered (hence between the beginning of Tezos mainnet on June 30th 2018 at 8:00:00 and the current time). Filtering using a range of dates is especially useful when exploring the biggest transactions, for instance to select either the biggest transactions ever made on the whole blockchain, or the biggest of the month, week, day, hour, etc. A new range of dates/times is taken into account once the update button is clicked.

Source and target: <img alt="UI screenshot" width="610" src="runtime/webui/help/img/menu-bar/srcandtrg.png"
          style="align-items: center; display:flex;"> (when searching for indirect transactions)

These fields enable to define the source and target addresses (which can be implicit accounts or smart contracts) of the sequence to be searched for. This makes it possible to search for indirect sequences of variable length (controlled by the maximum path length below) that may link the source address to the target address. The swap button in the middle (in between the source and target input fields) allows one to easily exchange the source and destination addresses (e.g. to look for reverse sequences).

Maximum path length: <img alt="UI screenshot" width="154" src="runtime/webui/help/img/menu-bar/maxpathlen.png" style="align-items: center; display:flex;"> (when searching for indirect transactions)

This field enables to specify the maximum length of the sequence to look for. It defines the total number of accounts in the longest sequence to look for. For instance, looking for a sequence with a maximum path length set to 3 means looking for sequences that link the aforementioned source and destination addresses by 2 hops in the graph (i.e. looking for 1 potential intermediate account between two given accounts, in a sequence of transactions linking the 3 accounts together). The default value for this parameter is 3. Warning: looking for arbitrarily deep sequences is a heavily computational task. It is recommended to use filters whenever possible in order to slash the combinatorics (for instance to restrict the search based on dates and/or on amounts).

Range of amount: <img alt="UI screenshot" width="295" src="runtime/webui/help/img/menu-bar/rangeofamount.png" style="align-items: center; display:flex;"> (when searching for indirect transactions)

This field is useful for restricting the search of indirect transactions so as to consider only transactions with an amount which is between the minimum and maximum values provided. By default, if the minimum and/or maximum values are not defined, transactions of any amount will be searched. The filters apply to all intermediate transactions, so filtering with tight bounds on the amount may prevent to find some transaction sequences, if there is at least one transaction in the sequence which does not match the filter.

Source account: <img alt="UI screenshot" width="293" src="runtime/webui/help/img/menu-bar/source.png" style="align-items: center; display:flex;">(when exploring from one account)

This field defines the source account to begin the exploration with. This address will be the origin marked in green in the [graph representation](graph-representation.md). This field is set automatically when the exploration is triggered by the "explore" button present on the property window. It can also be defined or updated manually at anytime.

Depth, number of transactions at level 1 and expansion ratio: <img alt="UI screenshot" width="579" src="runtime/webui/help/img/menu-bar/depthnbratio.png" style="align-items: center; display:flex;"> (when exploring from one account)

These parameters are used to control how the view of the graph starting from the source account should be recursively expanded. Depth is the maximum number of nested transactions to look for (i.e. the length of the longest sequence of transactions to look for when starting from the origin). The number of transactions at level 1 correspond to the maximum number x of transactions to be retained when starting from the origin (only the x transactions with biggest amount starting from the origin will be retained for further steps of expansion). The expansion (shrinking) ratio controls how many transactions should be expanded at level n based on the number of transactions expanded at level n-1. This affects a maximum number of transactions (i.e. only an upper bound on the number of transactions). For instance, when the expansion ratio is set to 0.4, and the initial number of transactions is set to 100, this means that at each recursive navigation step (i.e. at each hop), the maximum number of transactions retained is 40% of the maximum number of transactions retained at the previous step (so at level 2 no more than 40 transactions will be retained, no more than 16 at level 3, etc.). The expansion ratio makes it possible to control the balance in exploration granularity. For instance, setting an expansion ratio to a value greater than 1 favors more granularity when exploring deeper nested transactions. Setting an expansion ratio lower than 1 favors a finer-grained exploration of the initial transactions. By default this parameter is set to 0.5.