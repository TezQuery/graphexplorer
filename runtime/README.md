# Graph Explorer Runtime

This folder contains all the tools required to run the TezQuery blockchain graph
explorer.

## Execution in Docker

We provide 2 Docker composition files to run this architecture: a "production
one" and a "development one":

* the *production* one builds images from the current state of the repository
  and run it.
* the *development* one builds images with more development tools (`mypy`,
  `flake8`, `black`, ...) and mounts the `server` and `webui` local directories
  in the containers.

### Development mode

**Note:** On Windows, the Powershell scripts require WSL 1 to be installed. When
using WSL 2, you should install Docker inside it and use the Bash scripts
instead.

1. Run the `run_dev.sh` script (or `run_dev.ps1` on Windows), it will:
   1. Download the TzKT snapshot if necessary (~1.5G)
   2. Start the PostgreSQL database
   3. Load the TzKT snapshot
   4. Index the database
   5. Build images & start the composition
2. Once and for all, in VS Code, with the [Remote
   Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
   extension installed:
   1. Open the command palette (`Ctrl+Shift+P`) and select *Remote Containers:
      Open Attached Container Configuration File...*
   2. Select `runtime_server`
   3. Replace its content with:
   ```json
   {
       "workspaceFolder": "/opt/tezos_querier",
       "extensions": [
           "ms-python.python"
       ],
       "settings": {
           "terminal.integrated.shell.linux": "/bin/bash",
           "python.pythonPath": "/usr/local/bin/python",
           "python.linting.enabled": true,
           "python.linting.pylintEnabled": true,
           "python.formatting.autopep8Path": "/usr/local/py-utils/bin/autopep8",
           "python.formatting.blackPath": "/usr/local/py-utils/bin/black",
           "python.formatting.yapfPath": "/usr/local/py-utils/bin/yapf",
           "python.linting.banditPath": "/usr/local/py-utils/bin/bandit",
           "python.linting.flake8Path": "/usr/local/py-utils/bin/flake8",
           "python.linting.mypyPath": "/usr/local/py-utils/bin/mypy",
           "python.linting.pycodestylePath": "/usr/local/py-utils/bin/pycodestyle",
           "python.linting.pydocstylePath": "/usr/local/py-utils/bin/pydocstyle",
           "python.linting.pylintPath": "/usr/local/py-utils/bin/pylint"
       }
   }
   ```
3. Each time you want to work on the server, with VS Code and the remote
   containers extension:
   1. Display the list of running containers (Docker logo on the side bar)
   2. Right click on the `runtime_server` container and select *Attach Visual
      Studio Code*
   3. In a terminal, run the server you want:
      * Synchronous mode: `./entrypoint.sh flask`
      * Asynchronous mode: `./entrypoint.sh async`

      You can kill and restart those servers at will: the container life cycle
      is not based on the server in development mode.

## Architecture

The runtime is based on various parts:

* PostgreSQL/TzKT database
* TzKT synchronizer
* a Python HTTP server to query the database
* a Web UI

### TzKT

The `tzkt` folder contains the [TzKT Tezos indexer project](https://tzkt.io/).

This folder is a snapshot of its [GitHub
repository](https://github.com/baking-bad/tzkt/), at commit
`1b60a7804dae7d4751afcdb19ff53263c3dd3a1c`, "Update README.md", by `Groxan`.

It provides a `docker-compose` file that will start PostgreSQL database, the
synchronizer and a the REST API server. It is not recommended to use this
composition, as we provide a startup script in the folder of this file.

Notice that the TzKT indexer is used without modification of its source code.
This contributes to shield the Tezquery graph explorer from the evolution of the
transaction protocol, with the idea that the TzKT subcomponent can be safely
replaced by a newer version with limited impact on Tezquery. The only adaptation
done is that unused postgreSQL indexes are removed to diminish the PostgreSQL
database memory consumption (this is done automatically by the automated startup
scripts that we provide). 


### Server

The `server` folder provides a Python HTTP server that accepts user-formulated
queries as input, that queries the TzKT database and returns the results as
JSON.

Two version of the server exist: one based on
[Flask](https://flask.palletsprojects.com/) (synchronous) and one based on
[aiohttp](https://docs.aiohttp.org/) (asynchronous, possibly faster).

### WebUI

The `webui` folder contains the HTML/JS interface that can be used to send the
queries and display the results. It is based on `ggraph` and further builds on
it.

#### GGraph

The `webui/ggraph` folder contains a modified version of GGraph, with improved
and customized display and manipulation of the blockchain graph. The modified
ggraph subcomponent is initially based on the snapshot of the project's [GitHub
repository](https://github.com/pcbje/ggraph), from commit
`04ad66a7bc6e7d79677b51c81bde17af97c87264`, "Merge pull request #5 from
pcbje/dev", by Petter Christian Bjelland.





