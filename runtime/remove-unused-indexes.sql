-- Drop TzKT indexes not used in TezQuery:

--TransactionOps
DROP INDEX "IX_TransactionOps_OpHash";
DROP INDEX "IX_TransactionOps_Level";
DROP INDEX "IX_TransactionOps_InitiatorId";

-- Accounts
DROP INDEX "IX_Accounts_FirstLevel";
DROP INDEX "IX_Accounts_CreatorId";
DROP INDEX "IX_Accounts_ManagerId";
DROP INDEX "IX_Accounts_Type_Kind";


-- EndorsementOps
DROP INDEX "IX_EndorsementOps_OpHash";
DROP INDEX "IX_EndorsementOps_Level";
DROP INDEX "IX_EndorsementOps_DelegateId";

-- BakingRights
DROP INDEX "IX_BakingRights_Cycle_BakerId";

DROP INDEX "IX_DelegatorCycles_DelegatorId";
DROP INDEX "IX_DelegatorCycles_Cycle_BakerId";
DROP INDEX "IX_DelegatorCycles_Cycle";

-- DelegationOps
DROP INDEX "IX_DelegationOps_OpHash";
DROP INDEX "IX_DelegationOps_SenderId";
DROP INDEX "IX_DelegationOps_DelegateId";
DROP INDEX "IX_DelegationOps_PrevDelegateId";
DROP INDEX "IX_DelegationOps_Level";
DROP INDEX "IX_DelegationOps_InitiatorId";

-- OriginationOps
DROP INDEX "IX_OriginationOps_OpHash";
DROP INDEX "IX_OriginationOps_DelegateId";
DROP INDEX "IX_OriginationOps_ManagerId";
DROP INDEX "IX_OriginationOps_SenderId";
DROP INDEX "IX_OriginationOps_InitiatorId";
DROP INDEX "IX_OriginationOps_Level";

-- NonceRevelationOps
DROP INDEX "IX_NonceRevelationOps_OpHash";
DROP INDEX "IX_NonceRevelationOps_SenderId";
DROP INDEX "IX_NonceRevelationOps_BakerId";
DROP INDEX "IX_NonceRevelationOps_Level";


-- Blocks
DROP INDEX "IX_Blocks_ProtoCode";

-- BakerCycles
DROP INDEX "IX_BakerCycles_BakerId";



-- ProposalOps
DROP INDEX "IX_ProposalOps_OpHash";
DROP INDEX "IX_ProposalOps_Level";
DROP INDEX "IX_ProposalOps_SenderId";
DROP INDEX "IX_ProposalOps_ProposalId";


-- DoubleBakingOps
DROP INDEX "IX_DoubleBakingOps_Level";
DROP INDEX "IX_DoubleBakingOps_AccuserId";
DROP INDEX "IX_DoubleBakingOps_OpHash";
DROP INDEX "IX_DoubleBakingOps_OffenderId";

-- RevealOps
DROP INDEX "IX_RevealOps_OpHash";
DROP INDEX "IX_RevealOps_SenderId";
DROP INDEX "IX_RevealOps_Level";


-- ActivationOps
DROP INDEX "IX_ActivationOps_OpHash";
DROP INDEX "IX_ActivationOps_Level";


-- BallotOps
DROP INDEX "IX_BallotOps_SenderId";
DROP INDEX "IX_BallotOps_ProposalId";
DROP INDEX "IX_BallotOps_Level";
DROP INDEX "IX_BallotOps_OpHash";

-- DoubleEndorsingOps
DROP INDEX "IX_DoubleEndorsingOps_OpHash";
DROP INDEX "IX_DoubleEndorsingOps_AccuserId";
DROP INDEX "IX_DoubleEndorsingOps_Level";
DROP INDEX "IX_DoubleEndorsingOps_OffenderId";


-- RevelationPenaltyOps
DROP INDEX "IX_RevelationPenaltyOps_Level";
DROP INDEX "IX_RevelationPenaltyOps_BakerId";


-- MigrationOps
DROP INDEX "IX_MigrationOps_Level";
DROP INDEX "IX_MigrationOps_AccountId";

-- BallotOps
DROP INDEX "IX_BallotOps_Period";
DROP INDEX "IX_BallotOps_Epoch";

-- ProposalOps
DROP INDEX "IX_ProposalOps_Period";
DROP INDEX "IX_ProposalOps_Epoch";

-- Proposals
DROP INDEX "IX_Proposals_Hash";
DROP INDEX "IX_Proposals_Epoch";

-- VotingPeriods
DROP INDEX "IX_VotingPeriods_Epoch";

-- VotingSnapshots
DROP INDEX "IX_VotingSnapshots_Period";

-- Blocks
DROP INDEX "IX_Blocks_BakerId";
DROP INDEX "IX_Blocks_SoftwareId";

-- Storage
DROP INDEX "IX_Storages_ContractId";

DROP INDEX "IX_OriginationOps_ScriptId";