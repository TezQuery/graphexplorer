#!/bin/bash

snapFile="tzkt_db.backup"
if [[ ! -f "$snapFile" ]]
then
    echo "Downloading snapshot..."
    wget "https://tzkt-snapshots.s3.eu-central-1.amazonaws.com/tzkt_v1.5.backup" -O "$snapFile"
else
    echo "Reusing snapshot file"
fi

echo "Starting database"
docker-compose up -d db

echo "Setting up database"
docker-compose exec -T db psql -U tzkt postgres -c '\l'
docker-compose exec -T db dropdb -U tzkt --if-exists tzkt_db
docker-compose exec -T db createdb -U tzkt -T template0 tzkt_db

echo "Loading snapshot..."
docker-compose exec -T db pg_restore -U tzkt -O -x -v -d tzkt_db -1 < "$snapFile"

echo "Dropping unused indexes"
dropFile="remove-unused-indexes.sql"
docker-compose exec -T db psql -U tzkt -d tzkt_db < "$dropFile"

echo "Build images"
docker-compose build

echo "Start components"
docker-compose up -d
