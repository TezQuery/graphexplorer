$snapFile="tzkt_db.backup"
if (-Not (Test-Path $snapFile -PathType leaf))
{
    Write-Output "Downloading snapshot..."
    wsl wget -O $snapFile "https://tzkt-snapshots.s3.eu-central-1.amazonaws.com/tzkt_v1.5.backup"
}
else
{
    Write-Output "Reusing snapshot file"
}

$composeFile="docker-compose-dev.yml"

Write-Output "Starting database"
docker-compose -f "$composeFile" up -d db

Write-Output "Setting up database"
docker-compose -f "$composeFile" exec -T db psql -U tzkt postgres -c '\l'
docker-compose -f "$composeFile" exec -T db dropdb -U tzkt --if-exists tzkt_db
docker-compose -f "$composeFile" exec -T db createdb -U tzkt -T template0 tzkt_db

Write-Output "Loading snapshot..."
wsl bash -lic "docker-compose -f $composeFile exec -T db pg_restore -U tzkt -O -x -v -d tzkt_db -1 < $snapFile"

Write-Output "Dropping unused indexes"
$dropFile="remove-unused-indexes.sql"
wsl bash -lic "docker-compose exec -T db psql -U tzkt -d tzkt_db < $dropFile"

Write-Output "Build images"
docker-compose -f "$composeFile" build

Write-Output "Start components"
docker-compose -f "$composeFile" up -d
