# The RPQ server component

This is the server component whose goal is to get a query (e.g. an RPQ) as input, to process it, typically by issuing calls to the PostgreSQL database component, and to return results as JSON (typically further interpreted by the WebUI component).

## Pre-requisites

* [Flask](https://palletsprojects.com/p/flask/)
* [psycopg](https://www.psycopg.org/)

Those can be installed using: `pip install -r requirements.txt`

## Running

In the folder of this README file:
```bash
FLASK_APP=tezos_querier.server FLASK_ENV=development flask run -p 8000
```
