#!/bin/bash

case "$1" in
    "dev")
        echo "Continuous sleep..."
        sleep inf
        exit 0
        ;;

    "bash")
        echo "Running shell..."
        bash -i
        exit $?
        ;;

    "async")
        echo "Running server in asynchronous mode..."
        python -m tezos_querier.server_async -p 8000
        exit $?
        ;;

    "flask")
        echo "Running server with Flask..."
        flask run -h 0.0.0.0 -p 8000
        exit $?
        ;;

    *)
        echo "Running server..."
        gunicorn -b ":8000" "tezos_querier.server:app"
        exit $?
        ;;
esac
