#!/usr/bin/env python3
"""
Definition of the common beans
"""

from dataclasses import dataclass
from datetime import datetime, timezone
from enum import Enum, IntEnum
from typing import Any, Dict, Optional


class NodeType(IntEnum):
    """
    Defines the possible types of nodes as integers
    """
    IMPLICIT = 1
    ORIGINATED = 2
    ANONYMOUS = 3
    UNKNOWN = 4
    EXPLORATION_ORIGIN = 5 


@dataclass(frozen=True)
class Node:
    name: str

    def json(self,origin=None) -> Dict[str, Any]:
        if not self.name:
            node_type = "ANONYMOUS"
            node_type_int = NodeType.ANONYMOUS
        elif (origin is not None) and (self.name == origin):
            node_type = "ExploredAddress"
            node_type_int = NodeType.EXPLORATION_ORIGIN
        elif self.name[:3] in ("tz1", "tz2", "tz3"):
            node_type = "Implicit"
            node_type_int = NodeType.IMPLICIT
        elif self.name[:3] == "KT1":
            node_type = "Originated"
            node_type_int = NodeType.ORIGINATED
        else:
            node_type = f"Unknown:{self.name[:3]}"
            node_type_int = NodeType.UNKNOWN

        return {"id": self.name, "type_str": node_type, "type": node_type_int}
    
@dataclass(frozen=True)
class NodeID:
    node_id: int

    def json(self) -> Dict[str, Any]:
        return {"id": self.node_id, "type": "n/a"}


@dataclass(frozen=True)
class Link:
    timestamp: datetime
    source: str
    target: str
    value: Optional[int]

    def json(self) -> Dict[str, Any]:
        return {
            "timestamp": str(self.timestamp.astimezone(timezone.utc)),
            "source": self.source,
            "target": self.target,
            "value": self.value,
        }


@dataclass(frozen=True)
class IDLink:
    timestamp: datetime
    source: int
    target: int
    value: Optional[int]

    def json(self) -> Dict[str, Any]:
        return {
            "source": self.source,
            "target": self.target,
            "value": self.value,
        }


class SortColumn(Enum):
    """
    Defines the column to sort results by
    """

    TIMESTAMP = "Timestamp"
    AMOUNT = "Amount"
