#!/usr/bin/env python3
"""
Utility module to forge some basic SQL queries for TzKT
"""

from typing import Optional
import logging

from .beans import SortColumn


def transactions(
    limit: Optional[int] = 5,
    start: Optional[int] = None,
    min_timestamp: Optional[int] = None,
    max_timestamp: Optional[int] = None,
    order_by: SortColumn = SortColumn.TIMESTAMP,
    order_desc: bool = True,
) -> str:
    """
    Lists the latest transactions

    :param limit: Maximum number of transactions to return
    :param start: Start of the result pagination (if any)
    :param min_timestamp: Timestamp of the oldest transaction to return
                          (in milliseconds)
    :param max_timestamp: Timestamp of the newest transaction to return
                          (in milliseconds)
    :param order_by: Column to use to order results (Timestamp by default)
    :param order_desc: If True (default), order results in descending order
    :return: An SQL query string
    """
    query = """
    SELECT op."Timestamp",
        (SELECT "Address" AS Sender FROM "Accounts" a
            WHERE a."Id"=op."SenderId"),
        (SELECT "Address" AS Target FROM "Accounts" a
            WHERE a."Id"=op."TargetId"),
        op."Amount"
    FROM
        "TransactionOps" op
    WHERE
        op."Status" = 1
    """

    if min_timestamp is not None:
        query = f"""{query}
    AND
        op."Timestamp" >= to_timestamp({min_timestamp // 1000})
        """

    if max_timestamp is not None:
        query = f"""{query}
    AND
        op."Timestamp" <= to_timestamp({max_timestamp // 1000})
        """

    query += f"""
    ORDER BY
        op."{order_by.value}" {"DESC" if order_desc else "ASC"}
    """

    if limit is not None:
        query = f"{query} LIMIT {limit}"

    if start is not None:
        query = f"{query} OFFSET {start}"

    return query


def direct_transactions(
    source: str,
    target: str,
    limit: Optional[int] = None,
    start: Optional[int] = None,
) -> str:
    """
    SQL query to list direct transactions between 2 nodes

    :param source: Source node address
    :param target: Target node address
    :param limit: Maximum number of transactions to return
    :param start: Start of the result pagination (if any)
    :return: An SQL query string
    """
    query = f"""
    SELECT "Timestamp",
        (SELECT "Address" AS Sender FROM "Accounts" a
            WHERE a."Id"=op."SenderId"),
        (SELECT "Address" AS Target FROM "Accounts" a
            WHERE a."Id"=op."TargetId"),
        "Amount"
    FROM "TransactionOps" op
    WHERE
        op."SenderId" =
            (SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{source}')
    AND
        op."TargetId" =
            (SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{target}')
    AND
        op."Status" = 1
    ORDER BY "Timestamp" DESC
    """

    if limit is not None:
        query = f"{query} LIMIT {limit}"

    if start is not None:
        query = f"{query} OFFSET {start}"

    return query


def indirect_transactions_by_id(
    source_id: str,
    target_id: str,
    max_depth: Optional[int] = 3,
    limit: Optional[int] = None,
    start: Optional[int] = None,
) -> str:
    """
    SQL query to list indirect transactions between 2 nodes

    :param source_id: Source node **ID**
    :param target_id: Target node **ID**
    :param max_depth: Maximum length of the path between the two nodes
    :param limit: Maximum number of transactions to return
    :param start: Start of the result pagination (if any)
    :return: An SQL query string
    """
    query = f"""
WITH RECURSIVE "nodes" (
        "node",
        "depth",
        "path",
        "cycle",
        "timestamps",
        "amounts")
AS (
    SELECT
        {source_id},
        1,
        ARRAY[{source_id}],
        false,
        ARRAY[]::TIMESTAMP[],
        ARRAY[]::bigint[]
UNION ALL
    SELECT
        t."TargetId",
        n."depth" + 1,
        n."path" || t."TargetId",
        t."TargetId" = ANY(n."path"),
        "timestamps" || t."Timestamp",
        "amounts" || t."Amount"
    FROM
        "TransactionOps" t,
        "nodes" n
    WHERE
        t."SenderId" = n."node"
        AND t."Status" = 1
        AND NOT n."cycle"
        """

    # Add the filter on maximum depth
    if max_depth is not None:
        query = f"""{query}
        AND n."depth" < {max_depth}
        """

    query = f"""{query}
        AND t."Timestamp" >= COALESCE(
            "timestamps"[array_upper("timestamps", 1)],
            to_timestamp('1970-01-01 00:00:00', 'yyyy-mm-dd hh24-mi-ss')
        )
)
SELECT id_to_address("path") AS "path", "timestamps", "amounts" FROM (
    SELECT
        "node",
        min("depth"),
        "path",
        "cycle",
        "timestamps",
        "amounts"
    FROM
        "nodes"
    GROUP BY
        "node",
        "path",
        "cycle",
        "timestamps",
        "amounts"
    ) AS n
WHERE
    NOT "cycle"
    AND "node" = {target_id}
ORDER BY "timestamps"[array_upper("timestamps", 1)] DESC
    """

    if limit is not None:
        query = f"{query} LIMIT {limit}"

    if start is not None:
        query = f"{query} OFFSET {start}"

    return query


def create_id2addr_function() -> str:
    """
    Returns the SQL query to create the ``id_to_address`` function

    :return: The SQL code to create the ``id_to_address`` function
    """
    return """
DROP FUNCTION IF EXISTS id_to_address;

CREATE FUNCTION id_to_address(ids INTEGER[]) RETURNS bpchar[] AS
$body$
DECLARE
    node_id int;
    output bpchar[];
BEGIN
    FOREACH node_id IN ARRAY ids
    LOOP
        output := output || (
            SELECT a."Address" FROM "Accounts" a WHERE a."Id" = node_id
        );
    END LOOP;
    RETURN output;
END;
$body$ LANGUAGE plpgsql;
"""


def indirect_transactions_by_address(
    source_addr: str,
    target_addr: str,
    max_depth: Optional[int] = 3,
    min_timestamp: Optional[int] = None,
    max_timestamp: Optional[int] = None,
    min_amount: Optional[int] = None,
    max_amount: Optional[int] = None,
    limit: Optional[int] = None,
    start: Optional[int] = None,
) -> str:
    """
    SQL query to list indirect transactions between 2 nodes

    :param source_addr: Source node **address**
    :param target_addr: Target node **address**
    :param max_depth: Maximum length of the path between the two nodes
    :param min_timestamp: Optional minimum timestamp of the returned
                          transactions (in milliseconds)
    :param max_timestamp: Optional maximum timestamp of the returned
                          transactions (in milliseconds)
    :param min_amount: Optional minimum amount of the returned transactions
                       in millionth of Tezos
    :param max_amount: Optional maximum amount of the returned transactions
                       in millionth of Tezos
    :param limit: Maximum number of transactions to return
    :param start: Start of the result pagination (if any)
    :return: An SQL query string
    """
    query = f"""
WITH RECURSIVE "nodes" (
        "node",
        "depth",
        "path",
        "cycle",
        "timestamps",
        "amounts")
AS (
    SELECT
        (SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{source_addr}'),
        1,
        ARRAY[(
            SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{source_addr}'
        )],
        false,
        ARRAY[]::TIMESTAMP[],
        ARRAY[]::bigint[]
UNION ALL
    SELECT
        t."TargetId",
        n."depth" + 1,
        n."path" || t."TargetId",
        t."TargetId" = ANY(n."path"),
        "timestamps" || t."Timestamp",
        "amounts" || t."Amount"
    FROM
        "TransactionOps" t,
        "nodes" n
    WHERE
        t."SenderId" = n."node"
        AND t."Status" = 1
        AND NOT n."cycle"
        """

    # Add the filter on maximum depth
    if max_depth is not None:
        query = f"""{query}
        AND n."depth" < {max_depth}
        """

    # Prepare the filter on the minimal timestamp
    if min_timestamp is None:
        min_timestamp_sql: str = (
            "to_timestamp('1970-01-01 00:00:00', 'yyyy-mm-dd hh24-mi-ss')"
        )
    else:
        min_timestamp_sql = (
            f"to_timestamp(CAST({min_timestamp // 1000} as bigint))"
        )

    query = f"""{query}
        AND t."Timestamp" >= COALESCE(
            "timestamps"[array_upper("timestamps", 1)],
            {min_timestamp_sql}
        )
        """

    # Add the filter on the maximal timestamp, if any
    if max_timestamp is not None:
        query = f"""{query}
        AND t."Timestamp" <=
                to_timestamp(CAST({max_timestamp // 1000} as bigint))
        """

    # Add the filters on transaction amount
    if min_amount is not None:
        query = f"""{query}
        AND t."Amount" >= {min_amount}
        """

    if max_amount is not None:
        query = f"""{query}
        AND t."Amount" <= {max_amount}
        """

    query = f"""{query}
)
SELECT id_to_address("path") AS "path", "timestamps", "amounts" FROM (
    SELECT
        "node",
        min("depth"),
        "path",
        "cycle",
        "timestamps",
        "amounts"
    FROM
        "nodes"
    GROUP BY
        "node",
        "path",
        "cycle",
        "timestamps",
        "amounts"
    ) AS n
WHERE
    NOT "cycle"
    AND "node" = (
        SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{target_addr}'
    )
ORDER BY "timestamps"[array_upper("timestamps", 1)] DESC
    """

    if limit is not None:
        query = f"{query} LIMIT {limit}"

    if start is not None:
        query = f"{query} OFFSET {start}"

    return query


def node_direct_transactions(
    source: str,
    outgoing: bool = True,
    limit: Optional[int] = None,
    start: Optional[int] = None,
) -> str:
    """
    SQL query to list direct transactions between 2 nodes

    :param source: Source node address
    :param target: Target node address
    :param limit: Maximum number of transactions to return
    :param start: Start of the result pagination (if any)
    :return: An SQL query string
    """
    query = f"""
    SELECT "Timestamp",
        (SELECT "Address" AS Sender FROM "Accounts" a
            WHERE a."Id"=op."SenderId"),
        (SELECT "Address" AS Target FROM "Accounts" a
            WHERE a."Id"=op."TargetId"),
        "Amount"
    FROM "TransactionOps" op
    WHERE
        op."{"SenderId" if outgoing else "TargetId"}" =
            (SELECT a."Id" FROM "Accounts" a WHERE a."Address" = '{source}')
    AND
        op."Status" = 1
    ORDER BY "Timestamp" DESC
    """

    if limit is not None:
        query = f"{query} LIMIT {limit}"

    if start is not None:
        query = f"{query} OFFSET {start}"

    return query


def account_balance(account: str) -> str:
    """
    Returns the **current** balance of the given account address

    :param account: Account address
    :return: The query to get the balance of the given account,
             in 10^-6 of Tezos
    """
    return f"""
    SELECT "Balance" FROM "Accounts" WHERE "Address" = '{account}'
    """


def follow_big_amounts(
    account: str,
    level1_limit: int = 100,
    nb_levels: int = 3,
    level_ratio: float = 0.5,
    min_timestamp: Optional[int] = None,
    max_timestamp: Optional[int] = None,
) -> str:
    """
    Returns the query to follow the biggest transactions from a given node
    for a given number of levels.
    The limit of returned transactions shrinks according to the ``level_ratio``
    argument (if < 1)

    :param account: Source account address
    :param level1_limit: Maximum number of transactions to return from the
                         source account
    :param nb_levels: Number of levels to explore (depth)
    :param level_ratio: Ratio to use to change (**shrink** please) the maximum
                        number of transactions to return from each account
                        from the lower level
    :param min_timestamp: Optionally filter out transactions older than this
                          timestamp (integer, in milliseconds)
    :param max_timestamp: Optionally filter out transactions newer than this
                          timestamp (integer, in milliseconds)
    :return: An SQL query
    :raise ValueError: Invalid level ratio (must be >0)
    """
    # Check arguments
    if level_ratio <= 0:
        raise ValueError(f"Invalid level ratio: {level_ratio}")

    if level_ratio > 1:
        logging.warning("Level ratio is growing: %f", level_ratio)

    # Construct the timestamp filter (empty by default)
    time_filter = ""
    if min_timestamp is not None:
        time_filter = f"""
    AND
        "Timestamp" >= to_timestamp({min_timestamp // 1000})"""

    if max_timestamp is not None:
        time_filter = f"""{time_filter}
    AND
        "Timestamp" <= to_timestamp({max_timestamp // 1000})"""

    # First part: biggest transactions from the source account
    levels = [
        f"""
    WITH level1 AS (
    SELECT "Timestamp",
        "TargetId" as TargetId,  -- Keep track of the TargetId for filtering
        (SELECT "Address" AS Sender FROM "Accounts" a
            WHERE a."Id"=op."SenderId"),
        (SELECT "Address" AS Target FROM "Accounts" a
            WHERE a."Id"=op."TargetId"),
        "Amount" as Amount
    FROM "TransactionOps" op
    WHERE
        op."SenderId" =
            (SELECT a."Id" FROM "Accounts" a
                WHERE a."Address" = '{account}')
    AND
        op."Status" = 1
    {time_filter}
    ORDER BY op."Amount" DESC
        LIMIT {level1_limit}
    )"""
    ]

    for level in range(2, nb_levels + 1):
        levels.append(
            f"""
    level{level} AS (
    SELECT "Timestamp",
        "TargetId" as TargetId,  -- Keep track of the TargetId for filtering
        (SELECT "Address" AS Sender FROM "Accounts" a
            WHERE a."Id"=op."SenderId"),
        (SELECT "Address" AS Target FROM "Accounts" a
            WHERE a."Id"=op."TargetId"),
        "Amount" as Amount
    FROM "TransactionOps" op
    WHERE
        op."SenderId" IN (SELECT TargetId FROM level{level - 1})
    AND
        op."Status" = 1
    {time_filter}
    ORDER BY op."Amount" DESC
        LIMIT {level1_limit * (level_ratio ** (level - 1))}
    )"""
        )

    # Construct the union
    union = "\n\tUNION\n\t".join(
        f"SELECT * FROM level{level}" for level in range(1, nb_levels + 1)
    )

    # Construct the query
    query=f"""
    {",".join(levels)}
    SELECT "Timestamp", Sender, Target, Amount FROM (
        {union}
    ORDER BY
        "Timestamp" DESC
    ) AS allLevels
    """
    
    return query
