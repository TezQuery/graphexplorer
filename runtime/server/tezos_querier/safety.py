#!/usr/bin/env python3
"""
Utility module to have a minimalist guardrail against wreckless usage
"""

from typing import Dict, Optional, Tuple
import json
import logging
import pathlib


class Safeguard:
    """
    Loads configuration on creation. Ensures a value in the right limits
    """

    def __init__(self):
        # Define the limits dictionary
        self.limits: Dict[str, Tuple[Optional[int], Optional[int]]] = {}

    def load(self, filename: str) -> None:
        """
        Loads the configuration

        :param filepath: Path of the configuration file
        """
        filepath = pathlib.Path(filename)
        if not filepath.exists():
            logging.error("File not found: %s", filepath)
            return

        with filepath.open("r") as fd:
            whole_conf = json.load(fd)

        for method, method_conf in whole_conf.items():
            for parameter, limits in method_conf.items():
                # Prepare the configuration key
                key = f"{method}.{parameter}"

                if isinstance(limits, list):
                    if len(limits) == 1:
                        # Single value in list: use it as a max value
                        self.limits[key] = (None, limits[0])
                    elif len(limits) == 2:
                        # Min/Max values: convert to a tuple
                        self.limits[key] = tuple(limits)  # type: ignore
                    else:
                        logging.error(
                            "Invalid limit for parameter '%s' of '%s'",
                            parameter,
                            method,
                        )
                else:
                    # Single value: use it as a max value
                    self.limits[key] = (None, limits)

    def normalize(
        self, key: str, value: Optional[int], allow_none=True
    ) -> Optional[int]:
        """
        Ensures that the given value is in the right bounds
        """
        if allow_none and value is None:
            # Basic check on None value
            return value

        try:
            # Look for bounds here
            min_value, max_value = self.limits[key]
        except KeyError:
            # No limits
            return value

        if value is None:
            # Return non-None value if None is forbidden
            return min_value if min_value is not None else max_value

        if min_value is not None and value < min_value:
            # Lower boundary
            return min_value

        if max_value is not None and value > max_value:
            # Upper boundary
            return max_value

        # All good
        return value
