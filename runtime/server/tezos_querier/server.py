#!/usr/bin/env python3
"""
Synchronous version of the Python request server, based on Flask and Psycog
"""

from typing import List, Optional, Set, cast
import configparser
import html
import sys
import textwrap
import urllib.parse

from flask import Flask, jsonify, request, g, url_for
import flask
import psycopg2

from tezos_querier.beans import Link, Node, IDLink, NodeID, SortColumn
from tezos_querier.safety import Safeguard
from tezos_querier.utils import pairwise, is_inside_container, find_git_commit
import tezos_querier.forged_queries as sql


# Module version
__version_info__ = (0, 0, 1)
__version__ = ".".join(str(x) for x in __version_info__)

###############################################################################
# Module-level code: 1/2 Flask initialization
###############################################################################


# Read configuration
config = configparser.ConfigParser()
config.read("db.ini")

# Start the Flask server
# App must be defined at module level for WSGI servers to use it
app = Flask(__name__)


###############################################################################
# Safeguard
###############################################################################

safeguard = Safeguard()
safeguard.load("safeguard.json")


###############################################################################
# DB functions
###############################################################################


def connect_db() -> psycopg2.extensions.connection:
    """
    Connects to the database, without relying on Flask context
    """
    if not is_inside_container():
        default_host = "localhost"
    else:
        # Name of the service in docker compose
        default_host = "db"

    return psycopg2.connect(
        host=config.get("db", "host", fallback=default_host),
        port=config.getint("db", "port", fallback=5432),
        dbname=config.get("db", "db", fallback="tzkt_db"),
        user=config.get("db", "user", fallback="tzkt"),
        password=config.get("db", "password", fallback="qwerty"),
    )


def get_db() -> psycopg2.extensions.connection:
    """
    Creates or returns the link to the PgSQL database and associates it to the
    request context
    """
    if "db" not in g:
        g.db = connect_db()

    return g.db


def register_functions():
    """
    Registers functions inside the database. Should be done before the Flask
    application is started.
    """
    with connect_db() as db, db.cursor() as cur:
        # Register the id_to_address function
        cur.execute(sql.create_id2addr_function())


def close_db():
    """
    Closes the database connection associated to the request context
    """
    db = g.pop("db", None)
    if db is not None:
        db.close()


###############################################################################
# Path handling methods
###############################################################################


def get_request_uint_arg(
    request, name: str, default: Optional[int] = None
) -> Optional[int]:
    """
    Reads given integer argument from the URL.

    Returns the parsed argument or the default one if absent.
    Returns None if the argument was given but negative.

    :param request: Flask Request object
    :param name: Name of the argument
    :param default: Value to return if the argument was absent
    :return: The parsed argument, the default value or None
    """
    try:
        max_results = int(request.args.get(name, default))
    except (ValueError, TypeError):
        return default
    else:
        if max_results < 0:
            return None

    return max_results


def get_request_float_arg(
    request, name: str, default: Optional[float] = None
) -> Optional[float]:
    """
    Reads given float argument from the URL.

    Returns the parsed argument or the default one if absent.

    :param request: Flask Request object
    :param name: Name of the argument
    :param default: Value to return if the argument was absent
    :return: The parsed argument or the default value
    """
    try:
        max_results = float(request.args.get(name, default))
    except (ValueError, TypeError):
        return default

    return max_results


@app.route("/version", methods=["GET"])
def version():
    """
    Returns the version of this server
    """
    result = {
        "version": __version__,
        "version_info": __version_info__,
        "server": {"engine": "flask", "version": flask.__version__},
    }

    # Look for a git repository
    commit = find_git_commit()
    if commit:
        # Found it
        result["commit"] = commit

    return jsonify(result)


@app.route("/api/v1/help")
def site_map() -> str:
    """
    Prints out the URL map as an HTML page
    """
    html_rows = []
    module = sys.modules[__name__]

    for rule in app.url_map.iter_rules():
        options = {arg: f"<{arg}>" for arg in rule.arguments}

        doc = "n/a"
        try:
            if isinstance(rule.endpoint, str):
                doc = textwrap.dedent(getattr(module, rule.endpoint).__doc__)
            elif rule.endpoint is not None:
                doc = rule.endpoint.__doc__
        except AttributeError:
            pass

        html_rows.append(
            f"""<tr>
    <td><code>{", ".join(sorted(rule.methods))}</code></td>
    <td><code>{html.escape(
        urllib.parse.unquote(url_for(rule.endpoint, **options))
    )}</code></td>
    <td><code>{rule.endpoint}</code></td>
    <td><pre>{doc}</pre></td>
</tr>
"""
        )

    html_table_body = "\n".join(html_rows)

    return f"""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TezQuery API</title>
</head>
<body>
<h1>API Endpoints</h2>
<table border="1">
    <thead>
        <th>HTTP Methods</th>
        <th>URL</th>
        <th>Python Handler</th>
        <th>Python help</th>
    </thead>
    <tbody>
        {html_table_body}
    </tbody>
</table>
</body>
</html>
"""


@app.route("/api/v1/graph", methods=["GET"])
def graph():
    """
    Returns the whole transaction graph as a set of nodes and a set of links.

    Accepts 2 parameters: "start" and "limit"
    """
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Check arguments
    offset = get_request_uint_arg(request, "start")
    max_results = safeguard.normalize(
        "transactions.limit", get_request_uint_arg(request, "limit", 5)
    )

    # Default values are based on current time -30 minutes
    max_timestamp = get_request_uint_arg(request, "maxDate", None)
    min_timestamp = get_request_uint_arg(request, "minDate", None)

    # Order by a column
    order_by = SortColumn(
        request.args.get("orderBy", SortColumn.TIMESTAMP.value)
    )

    # Prepare the query
    query = sql.transactions(
        max_results, offset, min_timestamp, max_timestamp, order_by
    )

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            cur.execute(query)

            # Get results bucket by bucket
            results = cur.fetchmany()
            while results:
                # For each result in the bucket
                for result in results:
                    # Explode result row
                    timestamp, sender, target, amount = result

                    # Keep nodes
                    nodes.add(Node(sender))
                    nodes.add(Node(target))

                    # Store link
                    links.append(Link(timestamp, sender, target, amount))

                # Look for new results
                results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@app.route("/api/v1/<account>/direct-to/<target>", methods=["GET"])
def direct_relation(account: str, target: str):
    """
    Returns the transactions between 2 accounts (order matters) as a set of
    nodes and a set of links.

    Accepts 2 parameters: "start" and "limit"
    """
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Check arguments
    offset = get_request_uint_arg(request, "start")
    max_results = safeguard.normalize(
        "transactions.limit", get_request_uint_arg(request, "limit", 5)
    )

    # Prepare the query
    query = sql.direct_transactions(
        account, target, limit=max_results, start=offset
    )

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            cur.execute(query)

            # Get results bucket by bucket
            results = cur.fetchmany()
            while results:
                # For each result in the bucket
                for result in results:
                    # Explode result row
                    timestamp, sender, target, amount = result

                    # Keep nodes
                    nodes.add(Node(sender))
                    nodes.add(Node(target))

                    # Store link
                    links.append(Link(timestamp, sender, target, amount))

                # Look for new results
                results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@app.route("/api/v1/<account>/indirect-to/by-id/<target>", methods=["GET"])
def indirect_relation_by_id(account: str, target: str):
    """
    Returns the graph of indirect relations based on node IDs,
    as a set of nodes and a set of links
    """
    # Where we'll keep track of the graph
    nodes: Set[NodeID] = set()
    links: List[IDLink] = []

    # Check arguments
    max_depth = safeguard.normalize(
        "indirect_transactions.depth", get_request_uint_arg(request, "depth", 3)
    )
    offset = get_request_uint_arg(request, "start")
    max_results = safeguard.normalize(
        "indirect_transactions.limit", get_request_uint_arg(request, "limit", 5)
    )

    # Prepare the query
    query = sql.indirect_transactions_by_id(
        account, target, max_depth, limit=max_results, start=offset
    )

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            cur.execute(query)

            # Get results bucket by bucket
            results = cur.fetchmany()
            while results:
                # For each result in the bucket
                for result in results:
                    # Explode result row
                    path, timestamps, amounts = result

                    # Store nodes
                    nodes.update({NodeID(node) for node in path})

                    # Store links
                    for (src, trg), timestamp, amount in zip(
                        pairwise(path), timestamps, amounts
                    ):
                        # Store the link
                        links.append(IDLink(timestamp, src, trg, amount))

                # Look for new results
                results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@app.route("/api/v1/<account>/indirect-to/<target>", methods=["GET"])
def indirect_relation_by_address(account: str, target: str):
    """
    Returns the graph of indirect relations based on node addresses,
    as a set of nodes and a set of links
    """
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: Set[Link] = set()

    # Check arguments
    max_depth = safeguard.normalize(
        "indirect_transactions.depth", get_request_uint_arg(request, "depth", 3)
    )

    min_timestamp = get_request_uint_arg(request, "min-timestamp")
    max_timestamp = get_request_uint_arg(request, "max-timestamp")

    min_amount = get_request_uint_arg(request, "min-amount")
    max_amount = get_request_uint_arg(request, "max-amount")

    offset = get_request_uint_arg(request, "start", 0)
    max_results = safeguard.normalize(
        "indirect_transactions.limit", get_request_uint_arg(request, "limit", 5)
    )

    # Prepare the query
    query = sql.indirect_transactions_by_address(
        account,
        target,
        max_depth=max_depth,
        min_timestamp=min_timestamp,
        max_timestamp=max_timestamp,
        min_amount=min_amount,
        max_amount=max_amount,
        limit=max_results,
        start=offset,
    )

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            cur.execute(query)

            # Get results bucket by bucket
            results = cur.fetchmany()
            while results:
                # For each result in the bucket
                for result in results:
                    # Explode result row
                    path, timestamps, amounts = result

                    # Store nodes
                    nodes.update({Node(node) for node in path})

                    # Store links
                    for (src, trg), timestamp, amount in zip(
                        pairwise(path), timestamps, amounts
                    ):
                        links.add(Link(timestamp, src, trg, amount))

                # Look for new results
                results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@app.route("/api/v1/<account>/direct-transactions", methods=["GET"])
def node_direct_transactions(account: str):
    """
    Returns all the direct transactions to and from a node
    """
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Check arguments
    offset = get_request_uint_arg(request, "start")
    max_results = safeguard.normalize(
        "direct_transactions.limit", get_request_uint_arg(request, "limit", 5)
    )

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            for outgoing in (True, False):
                # Prepare the query
                query = sql.node_direct_transactions(
                    account, outgoing, limit=max_results, start=offset
                )
                cur.execute(query)

                # Get results bucket by bucket
                results = cur.fetchmany()
                while results:
                    # For each result in the bucket
                    for result in results:
                        # Explode result row
                        timestamp, sender, target, amount = result

                        # Keep nodes
                        nodes.add(Node(sender))
                        nodes.add(Node(target))

                        # Store link
                        links.append(Link(timestamp, sender, target, amount))

                    # Look for new results
                    results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@app.route("/api/v1/<account>/balance")
def account_balance(account: str):
    """
    Returns the balance of the given account

    :param account: Account address
    """
    # Prepare the query
    query = sql.account_balance(account)

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            cur.execute(query)

            # Get results bucket by bucket
            result = cur.fetchone()

            # Explode result row
            balance = result[0]
    finally:
        close_db()

    # Return the result as JSON
    return jsonify({"account": account, "balance": balance})


@app.route("/api/v1/<account>/follow", defaults={"nb_levels": 3})
@app.route("/api/v1/<account>/follow/<int:nb_levels>")
def follow_big_amounts(
    account: str,
    nb_levels: int = 3,
):
    """
    Follows the biggest transactions from a given account

    :param account: Address of the source account
    :param nb_levels: Number of levels (depth) to follow
    """
    # Extract arguments
    level1_limit = cast(
        int,
        safeguard.normalize(
            "follow.limit1", get_request_uint_arg(request, "limit1", 100)
        ),
    )
    level_ratio = cast(float, get_request_float_arg(request, "ratio", 0.5))
    min_timestamp = get_request_uint_arg(request, "minDate", None)
    max_timestamp = get_request_uint_arg(request, "maxDate", None)

    # Construct the query
    query = sql.follow_big_amounts(
        account,
        level1_limit,
        nb_levels,
        level_ratio,
        min_timestamp,
        max_timestamp,
    )

    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Run the query
    try:
        db = get_db()
        with db.cursor() as cur:
            for outgoing in (True, False):
                # Prepare the query
                cur.execute(query)

                # Get results bucket by bucket
                results = cur.fetchmany()
                while results:
                    # For each result in the bucket
                    for result in results:
                        # Explode result row
                        timestamp, sender, target, amount = result

                        # Keep nodes
                        nodes.add(Node(sender))
                        nodes.add(Node(target))

                        # Store link
                        links.append(Link(timestamp, sender, target, amount))

                    # Look for new results
                    results = cur.fetchmany()
    finally:
        close_db()

    # Return the result as JSON
    return jsonify(
        {
            "nodes": [Node.json(node,account) for node in nodes],
            "links": [link.json() for link in links],
        }
    )


###############################################################################
# Module-level code: 2/2 DB initialization
###############################################################################


register_functions()
