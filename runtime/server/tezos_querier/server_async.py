#!/usr/bin/env python3
"""
Asynchronous version of the Python request server, based on aiohttp and aiopg
"""

from typing import List, Optional, Set, Tuple
import argparse
import asyncio
import configparser

from aiohttp import web
import aiohttp
import aiopg

from tezos_querier.beans import Link, Node, IDLink, NodeID
from tezos_querier.utils import pairwise, is_inside_container, find_git_commit
import tezos_querier.forged_queries as sql


# Module version
__version_info__ = (0, 0, 1)
__version__ = ".".join(str(x) for x in __version_info__)

###############################################################################
# Module-level code: 1/2 Preparation of the aiohttp route definitions
###############################################################################


# Routes definitions
routes = web.RouteTableDef()

###############################################################################
# DB functions
###############################################################################


async def get_db():
    if not is_inside_container():
        default_host = "localhost"
    else:
        # Name of the service in docker compose
        default_host = "db"

    # Read configuration
    config = configparser.ConfigParser()
    config.read("db.ini")

    return await aiopg.connect(
        host=config.get("db", "host", fallback=default_host),
        port=config.getint("db", "port", fallback=5432),
        dbname=config.get("db", "db", fallback="tzkt_db"),
        user=config.get("db", "user", fallback="tzkt"),
        password=config.get("db", "password", fallback="qwerty"),
    )


async def register_functions():
    """
    Registers functions inside the database. Should be done before the Flask
    application is started.
    """
    db = await get_db()
    try:
        async with db.cursor() as cur:
            # Register the id_to_address function
            await cur.execute(sql.create_id2addr_function())
    finally:
        await db.close()


###############################################################################
# Path handling methods
###############################################################################


def get_request_uint_arg(
    request, name: str, default: Optional[int] = None
) -> Optional[int]:
    """
    Reads given integer argument from the URL.

    Returns the parsed argument or the default one if absent.
    Returns None if the argument was given but negative.

    :param request: aiohttp Request object
    :param name: Name of the arugment
    :param default: Value to return if the argument was absent
    :return: The parsed argument, the default value or None
    """
    try:
        max_results = int(request.rel_url.query.get(name, default))
    except (ValueError, TypeError):
        return default
    else:
        if max_results < 0:
            return None

    return max_results


@routes.get("/version")
def version(_):
    """
    Returns the version of this server
    """
    result = {
        "version": __version__,
        "version_info": __version_info__,
        "server": {"engine": "aiohttp", "version": aiohttp.__version__},
    }

    # Look for a git repository
    commit = find_git_commit()
    if commit:
        # Found it
        result["commit"] = commit

    return web.json_response(result)


@routes.get("/graph")
async def graph(request):
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Check arguments
    offset = get_request_uint_arg(request, "start")
    max_results = get_request_uint_arg(request, "limit", 5)

    # Prepare the query
    query = sql.transactions(max_results, offset)

    # Run the query
    db = await get_db()
    try:
        async with db.cursor() as cur:
            await cur.execute(query)

            # Get results bucket by bucket
            async for result in cur:
                # Explode result row
                timestamp, sender, target, amount = result

                # Keep nodes
                nodes.update((Node(sender), Node(target)))

                # Store link
                links.append(Link(timestamp, sender, target, amount))
    finally:
        await db.close()

    # Return the result as JSON
    return web.json_response(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@routes.get("/direct/{source}/{target}")
async def direct_relation(request):
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Get query details
    source = request.match_info["source"]
    target = request.match_info["target"]

    # Check arguments
    offset = get_request_uint_arg(request, "start")
    max_results = get_request_uint_arg(request, "limit", 5)

    # Prepare the query
    query = sql.direct_transactions(
        source, target, limit=max_results, start=offset
    )

    # Run the query
    db = await get_db()
    try:
        async with db.cursor() as cur:
            await cur.execute(query)

            # Get results bucket by bucket
            async for result in cur:
                # Explode result row
                timestamp, sender, target, amount = result

                # Keep nodes
                nodes.update((Node(sender), Node(target)))

                # Store link
                links.append(Link(timestamp, sender, target, amount))
    finally:
        await db.close()

    # Return the result as JSON
    return web.json_response(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@routes.get("/indirect-by-id/{source}/{target}")
async def indirect_relation_by_id(request):
    """
    Returns the graph of indirect relations based on node IDs
    """
    # Where we'll keep track of the graph
    nodes: Set[NodeID] = set()
    links: List[IDLink] = []

    # Get query details
    source = request.match_info["source"]
    target = request.match_info["target"]

    # Check arguments
    max_depth = get_request_uint_arg(request, "depth", 3)
    offset = get_request_uint_arg(request, "start")
    max_results = get_request_uint_arg(request, "limit", 5)

    # Prepare the query
    query = sql.indirect_transactions_by_id(
        source, target, max_depth, limit=max_results, start=offset
    )

    # Run the query
    try:
        db = await get_db()
        async with db.cursor() as cur:
            await cur.execute(query)

            # Get results bucket by bucket
            async for result in cur:
                # Explode result row
                path, timestamps, amounts = result

                # Store nodes
                nodes.update({NodeID(node) for node in path})

                # Store links
                for (src, trg), timestamp, amount in zip(
                    pairwise(path), timestamps, amounts
                ):
                    # Store the link
                    links.append(IDLink(timestamp, src, trg, amount))
    finally:
        await db.close()

    # Return the result as JSON
    return web.json_response(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


@routes.get("/indirect/{source}/{target}")
async def indirect_relation_by_address(request):
    """
    Returns the graph of indirect relations based on node addresses
    """
    # Where we'll keep track of the graph
    nodes: Set[Node] = set()
    links: List[Link] = []

    # Get query details
    source = request.match_info["source"]
    target = request.match_info["target"]

    # Check arguments
    max_depth = get_request_uint_arg(request, "depth", 3)
    offset = get_request_uint_arg(request, "start")
    max_results = get_request_uint_arg(request, "limit", 5)

    # Prepare the query
    query = sql.indirect_transactions_by_address(
        source, target, max_depth, limit=max_results, start=offset
    )

    # Run the query
    db = await get_db()
    try:
        async with db.cursor() as cur:
            await cur.execute(query)

            # Get results bucket by bucket
            async for result in cur:
                # Explode result row
                path, timestamps, amounts = result

                # Store nodes
                nodes.update({Node(node) for node in path})

                # Store links
                for (src, trg), timestamp, amount in zip(
                    pairwise(path), timestamps, amounts
                ):
                    links.append(Link(timestamp, src, trg, amount))
    finally:
        await db.close()

    # Return the result as JSON
    return web.json_response(
        {
            "nodes": [node.json() for node in nodes],
            "links": [link.json() for link in links],
        }
    )


###############################################################################
# Module-level code: 2/2 Setup of the aiohttp server and DB functions
###############################################################################


# App must be defined at module level for WSGI servers to use it
app = web.Application()
app.router.add_routes(routes)

# Register the DB functions
asyncio.get_event_loop().run_until_complete(register_functions())


def main(argv=None):
    """
    Entry point
    """
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", type=int, help="Port to listen to")
    options = parser.parse_args(argv)

    # Run the app
    web.run_app(app, port=options.port)


if __name__ == "__main__":
    main()
