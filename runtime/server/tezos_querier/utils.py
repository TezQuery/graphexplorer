#!/usr/bin/env python3
"""
Utility methods
"""

from typing import Optional
import itertools
import pathlib


def pairwise(iterable):
    """
    Recipe from the official documentation of the ``itertools`` module

    s -> (s0,s1), (s1,s2), (s2, s3), ...
    """
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def is_inside_container() -> bool:
    """
    Tries to detect if the script is running inside a Docker container

    :return: True if we are in a Docker container
    """
    if pathlib.Path("/.dockerenv").exists():
        return True

    cgroup = pathlib.Path("/proc/self/cgroup")
    if cgroup.exists():
        with open(cgroup, "r") as cgroup_fd:
            for line in cgroup_fd:
                if "/docker/" in line:
                    return True

    return False


def find_git_commit(base_path: Optional[pathlib.Path] = None) -> Optional[str]:
    """
    Looks in parent folders until it finds the current git commit signature

    :param base_path: Path to start the search from
    :return: The signature of HEAD or None
    """
    # Look for a git repository
    if base_path is None:
        base_path = pathlib.Path()

    # Ensure we have an absolute path, else .parent won't work
    path = base_path.absolute()
    prev_path = None

    try:
        while path != prev_path:
            git_dir = path.joinpath(".git")
            if git_dir.is_dir():
                # Found the .git folder
                with (git_dir / "HEAD").open("r") as head:
                    ref = head.readline().split(" ")[-1].strip()

                with (git_dir / ref).open("r") as git_hash:
                    # Read the commit
                    return git_hash.readline().strip()

            # Check in the parent directory
            prev_path = path
            path = path.parent
    except IOError:
        # Something went wrong: ignore
        pass

    # Error or no value found
    return None
