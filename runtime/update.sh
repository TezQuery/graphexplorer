#!/bin/bash

echo "Stopping containers and removes containers:"
docker-compose down

echo "Rebuilding images if needed:"
docker-compose build

echo "Starting components in detached mode:"
docker-compose up -d
