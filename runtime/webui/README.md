# Web UI component

A set of JS / HTML webpages whose purpose is to:

* Allow the formulation of an input query
* Query the server component
* Plot the graph of transactions
* Provides controls to facilitate further graph exploration
