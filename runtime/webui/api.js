/**
 * Utility methods that prepares target URL by emulating the server-side API
 */


/**
 * Utility class to generate a URL with query parameters
 */
class URLFoundry {
    /**
     * Sets up the base URL
     * @param {String} aBaseUrl Base URL
     */
    constructor(aBaseUrl) {
        this.url = aBaseUrl;

        // First separator = beginning of the query parameters
        this.sep = "?";
    }

    /**
     * Adds a query parameter to the URL
     *
     * @param {String} name Name of the argument
     * @param {*} value Value of the argument
     */
    addArgument(name, value) {
        if(value != null && value != undefined && value != "") {
            this.url += this.sep
                + encodeURIComponent(name)
                + "=" + encodeURIComponent(value);

            // Follow-up parameter separator
            this.sep = "&";
        }
    }
}


/**
 * Returns the requested transactions
 *
 * @param {int} aLimit Maximum number of transactions to return
 * @param {int} aOffset Start of the result pagination
 * @param {int} aMinTimestamp Timestamp of the oldest transaction to return (in milliseconds)
 * @param {int} aMaxTimestamp Timestamp of the newest transaction to return (in milliseconds)
 * @param {String} aOrderBy Column to use to order results (Timestamp by default)
 * @param {boolean} aOrderDesc If True (default), order results in descending order
 * @returns The URL to get the requested transactions
 */
function transactions(
    {
        aLimit = 5, aOffset = null,
        aMinTimestamp = null, aMaxTimestamp = null,
        aOrderBy = null, aOrderDesc = null
    }
) {
    var url = new URLFoundry("/api/v1/graph");
    url.addArgument("limit", aLimit);
    url.addArgument("start", aOffset);
    url.addArgument("minDate", aMinTimestamp);
    url.addArgument("maxDate", aMaxTimestamp);
    url.addArgument("orderBy", aOrderBy);
    url.addArgument("orderDesc", aOrderDesc);
    return url.url;
}


/**
 * Returns the URL to get the balance of the given account
 *
 * @param {String} aAccountAddress Address of an account
 * @returns The local URL to get the account balance
 */
function balance(aAccountAddress) {
    return "/api/v1/" + aAccountAddress + "/balance";
}


/**
 * Function to fetch metadata from a given list of addresses
 *
 * @param {List} addresses list of addresses of accounts
 * @returns A promise that will fulfil only if and when all promises in the array fulfil
 */
function fetchMetaData(addresses) { 
    try {
        // Retain previous bindings if any otherwise create new ones:    
        if (window.metadataBindings==undefined) {
            window.metadataBindings= new Map();
        }
            
        // Gather only addresses for which metadata is not already known:
        var addresses_to_fetch = addresses.filter(a => !window.metadataBindings.has(a));

        // This promise will fulfil only if and when all promises in the array fulfil
        var allprom = Promise.all(
            // Request metadata associated with each address:
            addresses_to_fetch.map(async address=> {
                return $.ajax({
                    type: "GET",
                    url: getmetadataURL(address)
                }).done(function (accountMetadata) {
                    // add a metadata entry
                    if (accountMetadata!=undefined) {
                        window.metadataBindings.set(address,accountMetadata)
                    } return 1;
                }).fail(function (xhr, status, error) {
                    return 0;
                });
            })
        );

        // Introduce a timeout (delay upperbound) to make sure that time needed for retrieving missing metadata does not delay too much the display (i.e the overall promise resolution of allprom):
        const timeout = (prom, time) => {
            let timer;
            return Promise.race([
                prom,
                new Promise((_r, rej) => timer = setTimeout(rej, time))
            ]).finally(() => clearTimeout(timer));
        }
        
        var max_delay_for_metadata= 500; //ms
        return timeout(allprom,max_delay_for_metadata)


    } catch (err) {
        console.log(err)
    }
}


/**
 * Returns the URL to get the metadata of a given account
 *
 * @param {String} address Address of an account
 * @returns The URL to get the account metadata
 */
 function getmetadataURL(address) { 
    return "https://api.tzkt.io/v1/accounts/"+address+"/metadata";
}




/**
 * Returns the direct transactions between two accounts
 *
 * @param {String} aSource Source account address
 * @param {String} aTarget Target account address
 * @param {int} aLimit Maximum number of returned transactions
 * @param {int} aOffset Offset of returned transactions (for pagination)
 * @returns {String} The URL to get the list of direct transactions
 */
function direct_transactions(
    {
        aSource, aTarget,
        aLimit = null, aOffset = null
    }
) {
    // Validate arguments
    if(!aSource || !aTarget) {
        throw new Exception("No source or target given.");
    }

    var url = new URLFoundry("/api/v1/" + aSource + "/direct-to/" + aTarget);
    url.addArgument("limit", aLimit);
    url.addArgument("start", aOffset);
    return url.url;
}


/**
  * Returns the URL to call for the list of indirect transactions
  *
  * @param {String} aSource Source account
  * @param {String} aTarget Target account
  * @param {int} aMaxDepth Maximum graph depth (expensive)
  * @param {int} aMinTimestamp Minimum epoch of transactions (in milliseconds)
  * @param {int} aMaxTimestamp Maximum epoch of transactions (in milliseconds)
  * @param {int} aMinAmount Minimum amount (in millionth of Tezos) of transactions
  * @param {int} aMaxAmount Maximum amount (in millionth of Tezos) of transactions
  * @param {int} aLimit Maximum number of returned transactions
  * @param {int} aOffset Offset of returned transactions (for pagination)
  * @returns {String} The URL to get the list of indirect transactions
  */
function indirect_by_address(
    {
        aSource, aTarget,
        aMaxDepth = 3,
        aMinTimestamp = null, aMaxTimestamp = null,
        aMinAmount = null, aMaxAmount = null,
        aLimit = null, aOffset = null
    }
) {
    // Validate arguments
    if(!aSource || !aTarget) {
        throw new Exception("No source or target given.");
    }

    var src_addr = aSource.startsWith("tz") || aSource.startsWith("KT1");
    var trg_addr = aTarget.startsWith("tz") || aTarget.startsWith("KT1");

    if (src_addr && trg_addr) {
        // Got 2 addresses
        var api_method = "indirect-to";
    } else if (!src_addr && !trg_addr) {
        // Got 2 IDs
        var api_method = "indirect-to/by-id";
    } else {
        alert("You must have 2 addresses or 2 IDs as source and target");
        return;
    }

    // Construct the URL
    var url = new URLFoundry("/api/v1/" + src + "/" + api_method + "/" + trg);
    url.addArgument("limit", aLimit);
    url.addArgument("start", aOffset);
    url.addArgument("depth", aMaxDepth);
    url.addArgument("min-timestamp", aMinTimestamp);
    url.addArgument("max-timestamp", aMaxTimestamp);
    url.addArgument("min-amount", aMinAmount);
    url.addArgument("max-amount", aMaxAmount);
    return url.url;
}


/**
 * Returns the URL to follow the biggest transactions from an account
 *
 * @param {String} aSourceAccount Source account address
 * @param {int} aNbLevels Number of levels to explore (depth)
 * @param {int} aLevel1Limit Maximum number of transactions to explore from source account
 * @param {float} aLevelRatio Ratio to change the maximum number of transactions to return from each account from the lower levels
 * @param {int} aMinTimestamp Timestamp of the oldest transaction (in milliseconds)
 * @param {int} aMaxTimestamp Timestamp of the newest transaction (in milliseconds)
 */
function follow_big_amounts(
    {
        aSourceAccount,
        aNbLevels = 3,
        aLimitLevel1 = 100,
        aLevelRatio = 0.5,
        aMinTimestamp = null,
        aMaxTimestamp = null
    }
) {

    // Validate arguments
    if(!aSourceAccount) {
        throw new Exception("No source account given.");
    }

    var url = new URLFoundry("/api/v1/" + aSourceAccount + "/follow/" + aNbLevels);
    url.addArgument("limit1", aLimitLevel1);
    url.addArgument("ratio", aLevelRatio);
    url.addArgument("minDate", aMinTimestamp);
    url.addArgument("maxDate", aMaxTimestamp);
    return url.url;
}
