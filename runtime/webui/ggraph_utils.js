/**
 * Utility methods from the ggraph demo that allows to merge, split, ...
 * the nodes in a graph
 */

function merge() {
    ggraph.merge(selection.all());
}

function remove() {
    var s = [];
    for (var k in selected) {
        s.push({ id: k })
    }
    ggraph.remove(s);
}

function split() {
    ggraph.split(all);
}

function _singles(e) {
    var groups = singles.get_groups(converted);
    all = groups;
    ggraph.merge(groups);
};

function _backbones(e) {
    var res = simmelian.filter(converted.all_links, 0.75);
    ggraph.filter_links(res);
};

/** Method for creating a new comment node and attaching it to a group of accounts: */
function _set_accountComment(comment) {
    if (selection.all().length === 0) return;

        var mock = ggraph.convert({
        nodes: [{id: comment, type: 'comment'}],
        links: []
      })
    
      var original = ggraph.get_graph();
      var comment_obj = mock.nodes[0];

      original.nodes.push(comment_obj);

     
      for (var key in mock.member_map) {
        original.member_map[key] = mock.member_map[key];
      }

      for (var key in mock.group_map) {
        original.group_map[key] = original.nodes.length - 1;
      }

      original.all_links[comment] = {}

      var tx = 0;
      var ty = 0;
       
      selection.all().map(function(selected) {
        var group_id = original.member_map[selected.id].group;
        var target = original.nodes[original.group_map[group_id]];
        tx += target.x;
        ty += target.y;

        original.all_links[comment][selected.id] = 1;
        original.links.push({
          source: comment_obj,
          target: target,
          value: 1
        })
      });

      comment_obj.x = tx / selection.all().length;
      comment_obj.y = ty / selection.all().length;

      ggraph.draw(original);
};