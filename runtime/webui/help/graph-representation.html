<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>TezQuery: Help</title>

    <!-- TezQuery CSS -->
    <link rel="stylesheet" href="../homebuttons.css" />
    <link rel="stylesheet" href="help.css" />
</head>

<body>
    <div id="mainpage">
        <h1>TezQuery Help - Graph Representation</h1>



        <h2>Basics</h2>
        <p>The TezQuery graph explorer exposes the Tezos blockchain as a graph data structure where accounts are linked
            by transactions.

            Two kinds of Tezos accounts can hold tokens and can be transaction source or destination: </p>
        <ul>
            <li> <span style="color:#1F77B4">implicit accounts</span> (addresses starting with "tz1", "tz2", or "tz3")
                that are represented as <span style="color:#1F77B4">blue nodes</span> in the graph;</li>

            <li><span style="color:#FF7F0E">smart contracts</span> (addresses starting with "KT1") that are represented
                as <span style="color:#FF7F0E">orange nodes</span> in the graph.</li>

        </ul>
        A transaction is seen as a directed edge between two accounts. It is represented as a <span
            style="color:#888888">gray arrow</span> starting from the source account (the sender) and pointing to the
        target account (the transaction destination). For instance:

        <figure class="legend">
            <table>
                <tr>
                    <td class="alnright"><img alt="graph pattern" style="width:80px;" src="img/graph-representation/pattern0.png" /></td>
                    <td class="figurelegend">A single transaction between an implicit account and a <span
                            style="color:#FF7F0E">smart contract</span>,
                        where the <span style="color:#1F77B4">implicit account</span> sends tokens to the <span
                            style="color:#FF7F0E">smart contract</span>.</td>
                </tr>

                <tr>
                    <td class="alnright"><img alt="graph pattern" style="width:120px;" src="img/graph-representation/pattern1.png" /></td>
                    <td class="figurelegend"> A pattern with 4 transactions in which one <span
                            style="color:#1F77B4">implicit account</span> (in the
                        middle) receives tokens from 3 different <span style="color:#FF7F0E">smart contracts</span>, and
                        sends tokens to another <span style="color:#1F77B4">implicit account</span>.</td>
                </tr>

                <tr>
                    <td class="alnright"><img alt="graph pattern" style="width:120px;" src="img/graph-representation/pattern2.png" /></td>
                    <td class="figurelegend">A pattern with 7 transactions between 8 <span
                            style="color:#1F77B4">implicit accounts</span>, where a single
                        account (in the middle) sends tokens to 7 other accounts.</td>
                </tr>
            </table>
        </figure>




        Clicking on an account in the graph displays more information such as the addresses of related accounts, and (in
        the property window) its own address, the current XTZ balance and metadata information when available.
        <img alt="property window" style="width:450px;" src="img/graph-representation/pw.png" />


        <h2 id="clusterrepresentation">Cluster representations</h2>

        When using <a href="exploration-kinds.html">advanced exploration features</a>, the graph explorer also uses
        cluster representations to simplify the layout and facilitate the graph exploration. Clusters can be either
        homogeneous (an homogeneous cluster regroups accounts of the same kind) or mixed (when it regroups at least one
        implicit account and one smart contract, and possibly more of each kind). For instance, you may encounter the
        following representations:

        <figure class="legend">
            <table>
                <tr>
                    <td class="alnright"> <img alt="graph pattern" style="width:80px;"
                            src="img/graph-representation/implicit-cluster2b.png" /></td>
                    <td class="figurelegend">Cluster regrouping 2 <span style="color:#1F77B4">implicit accounts</span>.
                    </td>
                </tr>

                <tr>
                    <td class="alnright"><img alt="graph pattern" style="width:120px;"
                            src="img/graph-representation/implicit-3-cluster.png" /></td>
                    <td class="figurelegend">Cluster of 3 <span style="color:#1F77B4">implicit accounts</span> from
                        which tokens were sent to another single
                        account.</td>
                </tr>

                <tr>
                    <td class="alnright"> <img alt="graph pattern" style="width:120px;"
                            src="img/graph-representation/implicit-4-cluster.png" /></td>
                    <td class="figurelegend">Cluster of 4 <span style="color:#1F77B4">implicit accounts</span> connected
                        (in both directions) to another
                        account.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:90px;" src="img/graph-representation/implicit-small-cluster.png" />
                    </td>
                    <td class="figurelegend">Cluster of 7 <span style="color:#1F77B4">implicit accounts</span>.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:120px;" src="img/graph-representation/implicit-medium-cluster.png" />
                    </td>
                    <td class="figurelegend">Homogeneous cluster of <span style="color:#1F77B4">implicit accounts</span>
                        with a transaction to another
                        account.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:120px;" src="img/graph-representation/homogeneous-cluster-smart.png" />
                    </td>
                    <td class="figurelegend">Homogeneous cluster of <span style="color:#FF7F0E">smart contracts</span>
                        with tokens sent to an <span style="color:#1F77B4">implicit account</span>.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:40px;" src="img/graph-representation/smart-2-mixed.png" />
                    </td>
                    <td class="figurelegend">Cluster of 2 <span style="color:#FF7F0E">smart contracts</span> with tokens
                        sent to an <span style="color:#1F77B4">implicit account</span>. </td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:110px;" src="img/graph-representation/heterogeneous-cluster24b.png" />
                    </td>
                    <td class="figurelegend">Mixed cluster containing 2 <span style="color:#FF7F0E">smart
                            contracts</span> and 4 <span style="color:#1F77B4">implicit accounts</span>.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:200px;" src="img/graph-representation/medium-mixed-cluster.png" />
                    </td>
                    <td class="figurelegend">Medium-size mixed cluster containing a rather balanced number of <span
                            style="color:#1F77B4">implicit
                            accounts</span> and <span style="color:#FF7F0E">smart contracts</span>, with transaction to
                        an <span style="color:#1F77B4">implicit account</span>.</td>
                </tr>

                <tr>
                    <td class="alnright">
                        <img alt="graph pattern" style="width:260px;" src="img/graph-representation/large-mixed-cluster.png" />
                    </td>
                    <td class="figurelegend">Large mixed cluster containing a high proportion of <span
                            style="color:#1F77B4">implicit accounts</span> and
                        few <span style="color:#FF7F0E">smart contracts</span>, with transaction to a single <span
                            style="color:#FF7F0E">smart contract</span>.</td>
                </tr>
            </table>

        </figure>







        Cluster representations can be formed automatically by clicking on the "<a
            href="menubar.html#groupsingles">group singles</a>" button on the main <a href="menubar.html">menu bar</a>.
        Custom clusters can also be created using the "<a href="menubar.html#select">select</a>" and then the "<a
            href="menubar.html#merge">merge</a>" tools of the menu bar. Any cluster can be
        split at anytime by using the "<a href="menubar.html#select">select</a>" and then the "<a
            href="menubar.html#split">split</a>" tools (see the <a href="menubar.html">menu bar help</a> for more
        details).


        <h2 id="origin">Exploration from one account</h2>
        When <a href="exploration-kinds.html#fromoneaccount">exploring from one account</a>, the origin of the
        exploration (which can be an account of any kind) is marked in green, for instance:

        <figure class="legend">
            <table>
                <tr>
                    <td class="alnright"> <img alt="graph pattern" style="width:300px;"
                            src="img/graph-representation/exploration-origin.png" /></td>
                    <td class="figurelegend">A sample Tezos graph with <span style="color:#2ca02c">exploration
                            origin</span>.</td>
                </tr>
            </table>

        </figure>

        <p>&nbsp;</p>
        <a href="index.html">&larr; <span style="font-size: smaller">TezQuery / Help</span></a>
    </div>
</body>

</html>