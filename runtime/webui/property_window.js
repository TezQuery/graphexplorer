/**
 * Set up the property window
 */

function createPropertyWindow() {

    // Parent panel
    var parentPanel = document.createElement("div");
    parentPanel.id = "propertyPanel";
    parentPanel.className = "ui-widget-content";

    // Tab container
    var tabs = document.createElement("div");
    tabs.id = "tabs";
    parentPanel.appendChild(tabs);

    // Tabs header definition
    var tabsUl = document.createElement("ul");
    tabs.appendChild(tabsUl);
    tabsUl.id = "tabsBar";

    for (var name of ["Account", "Transactions"]) {
        var link = document.createElement("a");
        var hrefAttr = document.createAttribute("href");
        hrefAttr.value = "#" + name.toLowerCase() + "Tab";
        link.setAttributeNode(hrefAttr);
        link.innerText = name;

        var item = document.createElement("li");
        item.appendChild(link);
        tabsUl.appendChild(item);
    }

    // Tabs content
    accountTabContent = document.createElement("div");
    tabs.appendChild(accountTabContent);
    accountTabContent.id = "accountTab";
    accountTabContent.className = "tabcontent";
    accountTabContent.innerHTML = '<p id="accountName" class="property"></p>'
        + '<p class="accountMetadata">'
        + '    <span id="accountAlias"></span>'
        + '    <span id="accountKind"></span>'
        + '</p>'
        + '<p id="accountDesc" class="property"></p>'
        + '<div id="exploreButton" data-role="main" class="ui-content"></div>'
        + '<p class="property">'
        + '    <span id="accountBalanceName"></span>'
        + '    <span id="accountBalance"></span>'
        + '</p>'
        + '<hr>'
        + '<p id="accountSummary" class="property"></p>';

    transactionTabContent = document.createElement("div");
    tabs.appendChild(transactionTabContent);
    transactionTabContent.id = "transactionsTab";
    transactionTabContent.className = "tabcontent";

    transactionTabContent.innerHTML = '<div id="data-table">'
        + '<table>'
        + '    <thead>'
        + '        <tr>'
        + '            <th>Timestamp</th>'
        + '            <th>Sender</th>'
        + '            <th>Target</th>'
        + '            <th>Amount</th>'
        + '        </tr>'
        + '    </thead>'
        + '    <tbody></tbody>'
        + '</table>'
        + '</div>';

    // Add the whole div to the body
    document.body.appendChild(parentPanel);

    // The property panel is draggable and resizable:
    $('#propertyPanel').css('boxShadow', '5px 5px 7px 3px gray');
    $('#propertyPanel').draggable({
        handle: "#tabsBar"
    });
    $('#propertyPanel').resizable({
        alsoResize: '.tabcontent',
        minWidth: 120,
        minHeight: 180
    });
    // Show the tabs:
    $(function () {
        $("#tabs").tabs();
    });

    /**
     * The previous HTML version:

    <!-- The property panel, draggable and resizable-->
    <div id="propertyPanel" class="ui-widget-content">
        <div id="tabs">
            <ul>
                <li><a href="#accountTab">Account</a></li>
                <li><a href="#transactionTab">Transactions</a></li>
            </ul>
            <!-- Tab for showing information about the account selected -->
            <div id="accountTab" class="tabcontent">

                <p id="accountName" class="property"></p>
                <p class="property">
                    <span id="accountBalanceName"></span>
                    <span id="accountBalance"></span>
                </p>
                <hr />
                <p id="accountSummary" class="property"></p>
            </div>

            <!-- Tab for showing information about the transactions selected (or all of them if none is selected) -->
            <div id="transactionTab" class="tabcontent">
                <div id="data-table">
                    <table>
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Sender</th>
                                <th>Target</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    *
    */
}


/**
 * Exploration function triggered by the explore button:
 */
function explore() {
    if (document.getElementById('accountName') != null) {
        accountAddress = document.getElementById('accountName').innerHTML

        // If already on the appropriate page then just (re)set the origin:
        srcField = document.getElementById("src");
        ratioField = document.getElementById("ratio");
        if (srcField != null && ratioField != null) {
            srcField.value = accountAddress;
            updateGraph();
        } else {
        // Otherwise, navigate to the exploration page with expected origin:
        window.location.href = "follow_transactions.html?src="+accountAddress;
        }
    }
}



/**
 * Clears the account description
 */
function clearAccountDescription() {
    document.getElementById('accountName').innerHTML = "";
    document.getElementById('accountAlias').innerHTML = "";
    document.getElementById('accountKind').innerHTML = "";
    document.getElementById('accountDesc').innerHTML = "";
    document.getElementById('exploreButton').innerHTML = "";
    document.getElementById('accountBalanceName').innerHTML = "";
    document.getElementById('accountBalance').innerHTML = "";
    document.getElementById("accountSummary").innerHTML = "";
}

/**
 * Clears the account description
 */
function accountDescriptionLoading(selectedAccount) {
    document.getElementById('accountName').innerHTML = selectedAccount;
    document.getElementById('exploreButton').innerHTML = '<button alt="Explore from this account" class="ui-btn" onclick="explore()">Explore</button>';
    document.getElementById('accountBalanceName').innerHTML = "Current balance: ";
    document.getElementById('accountBalance').innerHTML = "<i>loading</i>";
    document.getElementById("accountSummary").innerHTML = "";
}


/**
 * Updates the "balance" field of the account property window
 */
function updateAccountBalance(account_details, elementId = "accountBalance") {
    var value = "n/a"
    if (account_details != undefined) {
        value = "ꜩ&nbsp;" + account_details.balance / 1000000;
    }

    document.getElementById(elementId).innerHTML = value
}



/**
 * Updates the metadata fields of the account property window
 */
 function updateAccountMetadata(address, metadatabindings) {
    var account_metadata = metadatabindings.get(address);
    if (account_metadata != undefined) {

        var alias = account_metadata.alias
        var kind = account_metadata.kind
        var desc = account_metadata.description

        if (alias!=undefined) {
            document.getElementById("accountAlias").innerHTML = alias;
        }
        if (kind!=undefined) {
            document.getElementById("accountKind").innerHTML = " ("+kind+")";
        }

        if (desc!=undefined) {
            document.getElementById("accountDesc").innerHTML = desc;
        }
    }
}

/**
 * Updates the account activity summary information, containing e.g.
 * sums of incoming and outgoing transactions (not the balance and metadata part).
 */
function updateAccountActivitySummaryInfo(selectedAccount, fullGraph) {

    // Find the transactions for this account
    var outTransactions = fullGraph.links.filter(item => item.source == selectedAccount);
    var inTransactions = fullGraph.links.filter(item => item.target == selectedAccount);

    // Find all outgoing and incoming accounts
    var outNodes = new Set();
    outTransactions.forEach(element => {
        outNodes.add(element.target);
    });

    var inNodes = new Set();
    inTransactions.forEach(element => {
        inNodes.add(element.source);
    });

    // Sum up all transactions
    var outAmount = outTransactions.reduce((total, item) => total + item.value, 0) / 1000000;
    var inAmount = inTransactions.reduce((total, item) => total + item.value, 0) / 1000000;

    var descriptionHTML = "";

    if (inTransactions.length == 0) {
        descriptionHTML += "No incoming transaction.";
    } else {
        descriptionHTML += inTransactions.length
            + " incoming transaction" + pluralize(inTransactions.length)
            + " from " + inNodes.size
            + " account" + pluralize(inNodes.size)
            + " for a total of ꜩ&nbsp;"
            + inAmount
            + ".";
    }

    descriptionHTML += "<br><br>";

    if (outTransactions.length == 0) {
        descriptionHTML += "No outgoing transaction.";
    } else {
        descriptionHTML += outTransactions.length
            + " outgoing transaction" + pluralize(outTransactions.length)
            + " to " + outNodes.size
            + " account" + pluralize(outNodes.size)
            + " for a total of ꜩ&nbsp;"
            + outAmount
            + ".";
    }

    // Show the summary
    document.getElementById("accountSummary").innerHTML = descriptionHTML;
}


/**
 * Updates the property window (if any) with the selected account
 *
 * @param {String} aAccountAddress Address of an account
 * @param {*} aFullGraph The graph as returned by the /graph API call
 */
function updatePropertyWindow(aAccountAddress, aGraph) {
    // Async update of the balance
    accountDescriptionLoading(aAccountAddress);

    $.ajax({
        type: "GET",
        url: balance(aAccountAddress)
    }).done(function (account) {
        // Update the entry
        updateAccountBalance(account);
    }).fail(function (xhr, status, error) {
        // Update the entry
        updateAccountBalance(undefined);

        // Show the error
        var errorMessage = xhr.status + ": " + xhr.statusText;
        alert("Error: " + errorMessage);
    });

    updateAccountMetadata(aAccountAddress,window.metadataBindings)
   
    updateAccountActivitySummaryInfo(aAccountAddress, aGraph);
}
