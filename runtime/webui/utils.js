/**
 * Utility module of shared methods to handle results from the query server
 */


/**
 * Show the loading spinner
 */
function showLoading() {
    document.getElementById("container").style.visibility = "hidden";
    document.getElementById("load-spinner").style.visibility = "visible";
}

/**
 * Hide the loading spinner, show the graph
 */
function showGraph() {
    document.getElementById("load-spinner").style.visibility = "hidden";
    document.getElementById("container").style.visibility = "visible";
}

/**
 * Clears the graph
 */
function clearGraph() {
    // Remove the content
    graph = ggraph.convert({ "nodes": [], "links": [], "labels": [] });
    ggraph.draw(graph);
}

/**
 * Updates the table showing the transactions displayed in the graph.
 *
 * It needs a div of ID data-table, which contains a single table element
 */
function updateTransactionTable(graphLinks, tableParentId = "data-table") {
    // Clear the table first
    clearTransactionTable();

    // Get the table
    var table = document.getElementById(tableParentId).getElementsByTagName("table").item(0);

    // Prepare a new one
    var tbody = document.createElement("tbody");
    table.appendChild(tbody);

    graphLinks.forEach(transaction => {
        var row = document.createElement("tr");

        var timestamp = new Date(transaction.timestamp.replace(" ", "T"));
        var sender = transaction.source;
        var target = transaction.target;
        var amount = transaction.value / 1000000;

        var cell_timestamp = document.createElement("td");
        cell_timestamp.appendChild(document.createTextNode(timestamp.toLocaleString()));

        var cell_sender = document.createElement("td");
        cell_sender.appendChild(document.createTextNode(sender));

        var cell_target = document.createElement("td");
        cell_target.appendChild(document.createTextNode(target));

        var cell_amount = document.createElement("td");
        cell_amount.appendChild(document.createTextNode("ꜩ" + amount));

        row.appendChild(cell_timestamp);
        row.appendChild(cell_sender);
        row.appendChild(cell_target);
        row.appendChild(cell_amount);

        tbody.append(row);
    });

    try {
        // Convert it to a data table
        $("#data-table > table").DataTable({
            "order": [[0, "desc"]]
        });
    } catch {
        console.log("jQuery Datatables is missing");
    }
}

/**
 * Clears the content of the transaction table
 */
function clearTransactionTable(tableParentId = "data-table") {
    var table = document.getElementById(tableParentId).getElementsByTagName("table").item(0);

    // Remove the old body, if any
    var originalTBody = table.getElementsByTagName("tbody");
    if (originalTBody.length > 0) {
        table.removeChild(originalTBody.item(0));
    }
}

/**
 * Ensure the input integer as at least 2 digits
 */
function timeAddZero(value) {
    if (value >= 0 && value < 10) {
        return "0" + value;
    }
    return value;
}

/**
 * Converts a Javascript Date object to an input string for a datetime-local
 * input object
 */
function dateToDateTimeInput(date) {
    return date.getFullYear()
        + "-"
        + timeAddZero(date.getMonth() + 1)
        + "-"
        + timeAddZero(date.getDate())
        + "T"
        + timeAddZero(date.getHours())
        + ":"
        + timeAddZero(date.getMinutes())
        + ":"
        + timeAddZero(date.getSeconds());
}

/**
 * Adds the "s" suffix to plurals
 */
function pluralize(nb) {
    var str = ""
    if (nb > 1) {
        str += "s";
    }
    return str;
}

/**
 * Pads 0 in front of a date/time component:
 */
function pad(n, width = 2, z = 0) {
    return (String(z).repeat(width) + String(n)).slice(String(n).length);
}


/**
 * Sets the content of a date input field as best as we can
 *
 * @param {String} aFieldId ID of the date field
 * @param {Date} aDate A Date object
 */
function dateToInputField(aFieldId, aDate) {
    // Get the node
    var node = document.getElementById(aFieldId);
    if(!node) {
        console.error("Field doesn't exist: " + aFieldId);
        return;
    }

    if(!aDate) {
        // No date given: clear the value
        node.value = null;
        return;
    }

    // Get its type
    var nodeType = node.attributes["type"].value;

    // Try to use a specific method to set the value
    if(nodeType == "date") {
        node.value = dateToDateInput(aDate);
    } else if(nodeType == "time") {
        node.value = dateToTimeInput(aDate);
    } else if(nodeType == "datetime-local" || nodeType == "datetime") {
        node.value = dateToDateTimeInput(aDate);
    } else {
        node.value = aDate;
    }
}

/**
 * Converts a Javascript Date object to an input string for a date input object
 */
function dateToDateInput(date) {
    return date.getFullYear()
        + "-"
        + pad(date.getMonth() + 1) // month 0 is Jan
        + "-"
        + pad(date.getDate());
}

/**
 * Converts a Javascript Date object to an input string for a date time object
 */
function dateToTimeInput(date) {
    return pad(date.getHours())
        + ":"
        + pad(date.getMinutes())
        + ":"
        + pad(date.getSeconds());
}


/**
 * Returns the value of a field, if it exists, else a default value
 *
 * @param {String} aFieldId ID of the field to read
 * @param {*} aDefault Default value to return (undefined by default)
 * @returns The value of the given field, or a default value
 */
function safeGetFieldValue(aFieldId, aDefault = undefined) {
    if(aFieldId) {
        var node = document.getElementById(aFieldId);
        if(node) {
            return node.value;
        }
    }

    return aDefault;
}


/**
 * Reads the value of the given input fields (if they exist) and converts the
 * result into a Date object
 *
 * @param {String} aDateField Name of the input field of type "date"
 * @param {String} aTimeField Name of the input field of type "time"
 * @param {String} aDateTimeField Name of the input field of type "datetime-local"
 * @returns {Date} A Date object
 */
function dateFromInput(aDateField, aTimeField = undefined, aDateTimeField = undefined) {
    // Look for the date
    var dateFromField = safeGetFieldValue(aDateField);
    var timeFromField = safeGetFieldValue(aTimeField);
    var dateTimeFromField = safeGetFieldValue(aDateTimeField);

    if(dateFromField.includes("T")) {
        // First parameter was a datetime-local
        dateTimeFromField = dateFromField;
    }

    if(dateTimeFromField) {
        // Prepare the datetime-local field
        return new Date(dateTimeFromField);
    } else if(dateFromField) {
        // We got a valid date
        if(!timeFromField) {
            // No time given, consider midnight
            timeFromField = "00:00:00";
        }

        return new Date(dateFromField + "T" + timeFromField);
    } else {
        // Ignore the "time only" case
    }

    return undefined;
}


/**
 * Converts a Date object into an Epoch timestamp
 *
 * @param {Date} aDate A Date object
 * @returns The number of seconds (integer) since Epoch
 */
function dateToEpochSeconds(aDate) {
    return parseInt(aDate.getTime() / 1000);
}


/**
 * Swaps the value of two fields
 *
 * @param {String} aSource ID of the source field
 * @param {String} aTarget ID of the target field
 * @returns true if swap occurred
 */
function swapValues(aSource, aTarget) {
    var source = document.getElementById(aSource);
    var target = document.getElementById(aTarget);

    if(source && target) {
        var oldSrcValue = source.value;
        source.value = target.value;
        target.value = oldSrcValue;
        return true;
    }
    return false;
}


/**
 * Copies a value into the clipboard
 *
 * @param {*} aValue Value to copy
 * @param {String} aField ID of the holder field
 */
function copyToClipboard(aValue, aField = "copy") {
    var field = document.getElementById(aField);
    if(field) {
        copyField.value = aValue;
        copyField.select();
        copyField.setSelectionRange(0, 999999);
        document.execCommand("copy");
        copyField.value = "";
    }
}


/**
 * Sets the innerHTML of a field if it exists
 *
 * @param {String} aFieldId ID of the element to update
 * @param {*} aValue Value to set as innerHTML
 */
function safeSetInnerHTML(aFieldId, aValue) {
    if(aFieldId) {
        var node = document.getElementById(aFieldId);
        if(node) {
            node.innerHTML = aValue;
        } else {
            console.error("Node not found: " + aFieldId);
        }
    }
}

/**
 * Updates the nodes & edges count spans
 *
 * @param {Object} aGraph The graph as returned by the server
 * @param {String} aNodesId ID of the span showing the number of nodes
 * @param {String} aEdgesId ID of the span showing the number of edges
 */
function updateStats(aGraph, aNodesId = "node-count", aEdgesId = "edge-count") {
    var nodesCount = "n/a";
    var edgesCount = "n/a";

    if(aGraph) {
        nodesCount = aGraph.nodes.length;
        edgesCount = aGraph.links.length;
    }

    safeSetInnerHTML(aNodesId, nodesCount);
    safeSetInnerHTML(aEdgesId, edgesCount);
}


/**
 * Sets the value of a field for a query
 *
 * @param {String} aFieldId Field ID
 * @param {String} aParamName Name of a query parameter
 * @param {String} aValue optional default value
 */
function setFieldFromParam(aFieldId, aParamName, aValue=undefined) {
    // Check the field
    var field = document.getElementById(aFieldId);
    if(!field) {
        console.error("Unknown field ID: " + aFieldId);
        return;
    }

    // Check the value
    var value = new URLSearchParams(window.location.search).get(aParamName);
    if(value != null) {
        field.value = value;
    } else {
        if (aValue!=undefined) {
            field.value = aValue;
        }
    } 
}
